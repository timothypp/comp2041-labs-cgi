#!/usr/bin/perl -w

#
# Written by Timothy Putra Pringgondhani (z5043683)
# z5043683@zmail.unsw.edu.au
#
# Created on 13 October 2015
# Last Update on 25 October 2015 
#

#
# This CGI file helps to log the user out.
# This CGI does little logic and just set the cookie to empty and redirect the user
# back to the home page.
#

use strict;

use CGI qw/:all/;
use CGI::Cookie;
use CGI::Carp qw/fatalsToBrowser warningsToBrowser/;

use HTML;

sub main(){
  	# Now tell CGI::Carp to embed any warning in HTML
    warningsToBrowser(1);
    
    handle_page();
}

# This function helps to handle the page and do the appropriate logic here
sub handle_page{
    if (get_cookie('user') && get_cookie('auth_token')){ # Checks whether user is currently logged in or not. If not the page will be redirected to the home page.
		my $query = CGI->new;
		my $cookie_user = $query->cookie(-name=>'user', -value=>"");
		my $cookie_session_id = $query->cookie(-name=>'auth_token', -value=>"");

		print CGI::header('-cache-control' => 'NO-CACHE', -expires => -1, -pragma => 'pragma', -cookie=>[$cookie_user, $cookie_session_id]), 
			CGI::start_html(-head => meta( { -http_equiv => 'refresh',-content => '1;' } ) );
  	}
  	else{
      	print redirect(-url=>"./bitter.cgi");
  		#HTML::redirect_page("./bitter.cgi");
  	}
}

# Get the cookie value given a key
sub get_cookie{
    my $key = $_[0];
    my $value = "";

    my %cookies = fetch CGI::Cookie;

    if ($cookies{"$key"}){
        $value = $cookies{"$key"}->value;
    }

    return $value;
}

main();
