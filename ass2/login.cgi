#!/usr/bin/perl -w

#
# Written by Timothy Putra Pringgondhani (z5043683)
# z5043683@zmail.unsw.edu.au
#
# Created on 12 October 2015
# Last Update on 25 October 2015
#

#
# This CGI file helps to display the login page.
# This CGI file does small checking whether the appropriate information is filled in and
# sanitizes any information passed in.
#

use strict;

use CGI qw/:all/;
use CGI::Cookie;
use CGI::Carp qw/fatalsToBrowser warningsToBrowser/;

use Bitter;
use HTML;
use Security;

sub main(){
    # Build all of the Users' Data (Details & Bleats)
    Bitter::build_database();

	# Now tell CGI::Carp to embed any warning in HTML
    warningsToBrowser(1);
    
    handle_page();
}

# This function helps to handle the page and do the appropriate logic here
sub handle_page{
    my $user = get_cookie('user');
    my $session_id = get_cookie('auth_token');

    if ($user && $session_id && Security::check_user($user)){ # Checks whether user is currently logged in or not. If not the page will be redirected to the home page.
        print redirect(-url=>"./home.cgi");
        #HTML::redirect_page("./home.cgi");
    }
    else{
        my $login_id = param('log_id') || "";
        my $password = param('log_pass') || "";

        $login_id =~ substr(quotemeta $login_id, 0, 16); # Substring login id to 16 characters
        $login_id = Security::sanitize_data($login_id);
        $password = Security::sanitize_data($password);

        if (length($login_id) > 0 && length($password) > 0){
            my $result = Security::login_helper($login_id, $password);

            if ($result == 1){
                my $proper_username = Bitter::get_username_by_email($login_id);
                my $time_epoch = time;
                my $session_id = Security::encode_data_md5("$proper_username|$time_epoch");

                my $query = CGI->new;
                my $cookie_user = $query->cookie(-name=>'user', -value=>lc($proper_username));
                my $cookie_session_id = $query->cookie(-name=>'auth_token', -value=>$session_id);

    	    	print CGI::header('-cache-control' => 'NO-CACHE', -expires => -1, -pragma => 'pragma', -cookie=>[$cookie_user, $cookie_session_id]), 
                      CGI::start_html(-head => meta( { -http_equiv => 'refresh',-content => '1;' } ) );
            }
            elsif ($result == 2){
                print_page($login_id, $password, "Your account has not been activated yet. Please check your email to activate your account.");
            }
            else{
                print_page($login_id, $password, "The Login ID and Password that you entered did not match our records. Please double-check and try again.");
            }
        }
        else{
            print_page();
        }
    }
}

# This function helps to print out the login page.
sub print_page{
    my $login_id = $_[0] || "";
    my $password = $_[1] || "";
    my $alert_message = $_[2] || "";

    HTML::header("Bitter - Login");
    HTML::navigation("Login");
    HTML::build_login_page($login_id, $password, $alert_message);
    HTML::footer(HTML::build_check_login_script());
}

# Get the cookie value given a key
sub get_cookie{
    my $key = $_[0];
    my $value = "";

    my %cookies = fetch CGI::Cookie;

    if ($cookies{"$key"}){
        $value = $cookies{"$key"}->value;
    }

    return $value;
}

main();
