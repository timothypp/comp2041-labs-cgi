#!/usr/bin/perl -w

#
# Written by Timothy Putra Pringgondhani (z5043683)
# z5043683@zmail.unsw.edu.au
#
# Created on 12 October 2015
# Last Update on 25 October 2015
#

#
# This CGI file helps to display the sign up page.
# This CGI file does small checking whether the appropriate information is filled in and
# sanitizes any information passed in.
#

use strict;

use CGI qw/:all/;
use CGI::Cookie;
use CGI::Carp qw/fatalsToBrowser warningsToBrowser/;

use Bitter;
use HTML;
use Security;

sub main(){
    # Build all of the Users' Data (Details & Bleats)
    Bitter::build_database();

	# Now tell CGI::Carp to embed any warning in HTML
    warningsToBrowser(1);
    
    handle_page();
}

# This function helps to handle the page and do the appropriate logic here
sub handle_page{
    my $user = get_cookie('user');
    my $session_id = get_cookie('auth_token');

    if ($user && $session_id && Security::check_user($user)){ # Checks whether user is currently logged in or not. If not the page will be redirected to the home page.
        print redirect(-url=>"./home.cgi");
        #HTML::redirect_page("./home.cgi");
    }
    else{
        my $activate_code = param('activate') || "";
        my $activate_user = param('user') || "";

        # Compulsory
        my $username = param('su_user') || "";
        my $email = param('su_em') || "";
        my $password = param('su_pass') || "";

        # Additional
        my $full_name = param('fn') || "";
        my $latitude = param('lat') || "";
        my $longitude = param('long') || "";
        my $suburb = param('sub') || "";


        $activate_user =~ substr(quotemeta $activate_user, 0, 16); # Substring login id to 16 characters
        $activate_user = Security::sanitize_data($activate_user);
        $activate_code = Security::sanitize_data($activate_code);

        $username =~ substr(quotemeta $username, 0, 16); # Substring login id to 16 characters
        $username = Security::sanitize_data($username);
        $email = Security::sanitize_data($email);
        $password = Security::sanitize_data($password);

        $full_name = Security::sanitize_data($full_name);
        $latitude = Security::sanitize_data($latitude);
        $longitude = Security::sanitize_data($longitude);
        $suburb = Security::sanitize_data($suburb);

        if (length($activate_code) > 0 && length($activate_user) > 0){

            my $result = Bitter::activate_user("$activate_code", "$activate_user");

            if ($result == 0){
                print_activation_page("Account does not exists. Please double check that the account exists!", 1);
            }
            elsif ($result == 1){
                 print_activation_page("Thank you for activating your Account!", 0);
            }
            elsif ($result == 2){
                print_activation_page("Activation code is invalid. Please click the link in your email to activate your Account!", 1);
            }
            else{
                print_activation_page("Fail to activate your Account!", 1);
            }
        }
        elsif (length($username) > 0 && length($email) > 0 && length($password) > 0){
            
            if ($username !~ /^[A-Za-z0-9\-\_\.]+$/){
                print_page($username, $email, $password, $full_name, $latitude, $longitude, $suburb, "Please enter a valid username. Username must be Alphanumeric, Underscore, Dot or Dash !");
            }
            elsif($email !~ /^[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,}$/) {
                print_page($username, $email, $password, $full_name, $latitude, $longitude, $suburb, "Please enter a valid Email Address !");
            }
            elsif ($latitude !~ /^(-)?[0-9.]+$/ && length($latitude) > 0){
                print_page($username, $email, $password, $full_name, $latitude, $longitude, $suburb, "You have entered an invalid Location. Please enter the correct Latitude !");
            }
            elsif ($longitude !~ /^(-)?[0-9.]+$/ && length($longitude) > 0){
                print_page($username, $email, $password, $full_name, $latitude, $longitude, $suburb, "You have entered an invalid Location. Please enter the correct Longitude !");
            }
            else{
                my $url_link = $ENV{SCRIPT_URI};
                $url_link =~ s/\/$//;

                my $result = Bitter::register_new_user($username, $email, $password, $full_name, $latitude, $longitude, $suburb, $url_link);

                if ($result == 1){ # 1 : Success | 2 : Duplicate Username | Others : Failure
                    print_activation_page("We just send you an email to <b>$email</b>.<br/>Please activate your Account through the link provided in the email!", 0);
                }
                elsif ($result == 2){ 
                    print_page($username, $email, $password, $full_name, $latitude, $longitude, $suburb, "The Username exists, please choose another Username !");
                }
                elsif ($result == 3){ 
                    print_page($username, $email, $password, $full_name, $latitude, $longitude, $suburb, "The Email Address has been used before, please choose another Email Address !");
                }
                else{
                    print_page($username, $email, $password, $full_name, $latitude, $longitude, $suburb, "Failed to Sign you Up right now !");
                }
            }
        }
        else{
            print_page();
        }
    }
}

# This will print the sign up page.
sub print_page{
    my $username = $_[0] || "";
    my $email = $_[1] || "";
    my $password = $_[2] || "";

    my$full_name = $_[3] || "";
    my $latitude = $_[4] || "";
    my $longitude = $_[5] || "";;
    my $suburb = $_[6] || "";

    my $alert_message = $_[7] || "";

    HTML::header("Bitter - Sign Up");
    HTML::navigation("SignUp");
    
    HTML::build_signup_page($username, $email, $password, $full_name, $latitude, $longitude, $suburb, $alert_message);

    HTML::footer(HTML::build_check_sign_up_script(),  HTML::build_get_location_script());
}

# This function will print the activation page.
sub print_activation_page{
    my $alert_message = $_[0] || "";
    my $is_error = $_[1] || 0;

    HTML::header("Bitter - Sign Up");
    HTML::navigation("SignUp");
    
    HTML::build_verification_page($alert_message, $is_error);

    HTML::footer();
}

# Get the cookie value given a key
sub get_cookie{
    my $key = $_[0];
    my $value = "";

    my %cookies = fetch CGI::Cookie;

    if ($cookies{"$key"}){
        $value = $cookies{"$key"}->value;
    }

    return $value;
}

main();
