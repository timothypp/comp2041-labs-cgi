#!/usr/bin/perl -w

#
# Written by Timothy Putra Pringgondhani (z5043683)
# z5043683@zmail.unsw.edu.au
#
# Created on 12 October 2015
# Last Update on 25 October 2015
#

use strict;

use CGI qw/:all/;
use CGI::Carp qw/fatalsToBrowser warningsToBrowser/;

use Bitter;

sub main(){
	# Build all of the Users' Data (Details & Bleats)
	Bitter::build_database();

	# Now tell CGI::Carp to embed any warning in HTML
    warningsToBrowser(1);
    
    # Redirect User to Bitter.cgi
	print redirect(-url=>"./bitter.cgi");
	#HTML::redirect_page("./bitter.cgi");
}

main();
