#!/usr/bin/perl -w

#
# Written by Timothy Putra Pringgondhani (z5043683)
# z5043683@zmail.unsw.edu.au
#
# Created on 12 October 2015
# Last Update on 24 October 2015 10:22 PM
#

# This Perl Module is designed as a core for Bitter.
# This perl module stores all of the logic and acts as a backend for Bitter to do calculation.
#
# This module helps to create files, send emails, update information, listing bleats and many more
#

use strict;
use Storable;

use HTML; # Use HTML module
use Security; # User Security module

package Bitter;

our $dataset_size = "large"; # A global variable for the size of the dataset
our $users_dir = "dataset-$dataset_size/users"; # A global variable for a user directory
our $bleats_dir = "dataset-$dataset_size/bleats"; # A global variable for a bleat directory
our $upload_dir = "dataset-$dataset_size/uploads"; # A global variable for a uploaded file of bleat directory

our $no_profile = "assets/no_profile.jpg"; # A global variable for a default profile picture
our $default_background = "assets/default_back.jpg"; # A global variable for a default background image

# Hashes of Hashes
our %global_all_users; # All of the user's details in a hash
our %global_all_user_bleats; # All of the user's bleat in a hash
our %global_bitter; # Bitter Data in a hash

my $latest_bleat_id = 0; # The latest bleat ID used
my $result_limit = 16; # The number of result limited in a page
my $pagination_limit = 8; # The number of page number that is allowed to be shown

my $cache_file_user 	= "cache.user"; # The cache file name for All of the User's Details
my $cache_file_bleat 	= "cache.bleat"; # The cache file name for All of the User's Bleats
my $cache_file_bitter 	= "cache.bit"; # The cache file name for Bitter Data hash


# Build the database of the user
sub build_database{
	my $should_re_run_read = $_[0] || "yes"; # Determine whether the building databse should read the folder again

	if (-r $cache_file_user){
		%global_all_users = %{Storable::retrieve("$cache_file_user")};
	}
	else{
		get_all_users_details() if ($should_re_run_read eq "yes");
		get_a_user_bleats() if ($should_re_run_read eq "yes");

		Storable::store(\%global_all_users, "$cache_file_user");
	}


	if (-r $cache_file_bleat){
		%global_all_user_bleats = %{Storable::retrieve("$cache_file_bleat")};
	}
	else{
		get_all_users_bleats() if ($should_re_run_read eq "yes");

		Storable::store(\%global_all_user_bleats, "$cache_file_bleat");
		Storable::store(\%global_all_users, "$cache_file_user");
	}

	if (-r $cache_file_bitter){
		%global_bitter = %{Storable::retrieve("$cache_file_bitter")};
		$latest_bleat_id = $global_bitter{"latest_bleat_id"};
	}
	else{
	    mkdir "$upload_dir"; # Create the upload folder for bleats
	    chmod 0755, "$upload_dir"; # Change permission of the folder for bleats upload

		Storable::store(\%global_bitter, "$cache_file_bitter");
	}
}





# Delete the necessary files before doing database rebuild
sub update_database{
	my $databse_id = $_[0]; # 1: User, 2: Bleat, 3: Bitter General

	# Remove files that are needed to be updated
	if ($databse_id == 1){
		unlink $cache_file_user or return 0;
	}
	elsif ($databse_id == 2){
		unlink $cache_file_bleat or return 0;
	}
	elsif ($databse_id == 3){
		unlink $cache_file_bitter or return 0;
	}

	return 1;
}





# Create a Database that gets All of the Users Details
sub get_all_users_details{
	my @all_users = glob("$users_dir/*");

	for my $user_to_show (@all_users){
		my $details_filename = "$user_to_show/details.txt";

		my $username = $user_to_show;
		$username =~ s/$users_dir\///;
    	$username = remove_new_line_and_spaces($username);
		$username = lc($username);

	    open my $p, "$details_filename" or die "Bitter cannot open $details_filename: $!";
	   	
	    while (my $detail = <$p>) {
	    	$detail =~ /^(.*?):(.*)/;

	    	my $key = $1;	 	# Key of Hash
	    	$key = remove_new_line_and_spaces($key);

	    	my $value = $2;		# Value of Hash
	    	$value = remove_new_line_and_spaces($value);

	    	if ($key =~ /password/i){
	    		$value = Security::encode_data_md5($value);
	    	}
	    	
	    	if ($key =~ /listens/i){
	    		my @all_listens = split(" ", lc($value));
			    @all_listens = sort @all_listens;

			    if (scalar(@all_listens) == 0 || length($value) == 0){
			    	@all_listens = ();
			    }

			     s{^\s+|\s+$}{}g foreach @all_listens;

		    	$global_all_users{"$username"}{"listens"} = [ @all_listens ]; # Hashes of Hashes
		    	next;
	    	}
	    	else{
		    	$global_all_users{"$username"}{"$key"} = "$value"; # Hashes of Hashes
		    }
		}

	    close $p;

	    # Set User Profile Picture
	    if (-f "$user_to_show/profile.jpg"){
	    	$global_all_users{"$username"}{"profile_pic"} = "$user_to_show/profile.jpg"; # Hashes of Hashes
	    }
	    else{
	    	$global_all_users{"$username"}{"profile_pic"} = $no_profile; # Hashes of Hashes
	    }

	    # Set User Profile code
	    if (-f "$user_to_show/background.jpg"){
	    	$global_all_users{"$username"}{"background_pic"} = "$user_to_show/background.jpg"; # Hashes of Hashes
	    }
	    else{
	    	$global_all_users{"$username"}{"background_pic"} = $default_background; # Hashes of Hashes
	    }

	    my @mention_array = ();
	    $global_all_users{"$username"}{"mention_bleat"} = [ @mention_array ]; # Hashes of Hashes

	    $global_all_users{"$username"}{"status"} = "valid"; # Status : Verify (Sign Up Verification Required), Valid (Normal), Suspend (Invincible Account)
	    $global_all_users{"$username"}{"activation_code"} = ""; # Activation code for new user
	    $global_all_users{"$username"}{"recover_password_code"} = ""; # Activation code for new user
	

	    $global_all_users{"$username"}{"notify_bleat"} = "off"; # Should notify when someone mentioned in a bleat
	    $global_all_users{"$username"}{"notify_reply"} = "off"; # Should notify when someone reply user's bleat
	    $global_all_users{"$username"}{"notify_new_listener"} = "off"; # Should notify when gain new listener


		if (not defined $global_all_users{"$username"}{"profile_text"}){
		    $global_all_users{"$username"}{"profile_text"} = ""; # Profile Text
		}
	}
}





# Create a Database that gets All of the Users Bleats
sub get_a_user_bleats{
	my @all_users = glob("$users_dir/*");

	for my $user_to_show (@all_users){
		my $details_filename = "$user_to_show/bleats.txt";

		$user_to_show =~ s/$users_dir\///;
		$user_to_show = remove_new_line_and_spaces($user_to_show);
		$user_to_show = lc($user_to_show);

	    open my $p, "$details_filename" or die "Bitter cannot open $details_filename: $!";
	    
	    my @all_bleats = ();

	    while (my $bleat_id = <$p>) {
			$bleat_id = remove_new_line_and_spaces($bleat_id);
			$bleat_id = lc($bleat_id);

			push(@all_bleats, $bleat_id) if ($bleat_id);
	    }

	    @all_bleats = reverse sort @all_bleats;

	    if (scalar(@all_bleats) == 0){
	    	@all_bleats = ();
	    }

	    s{^\s+|\s+$}{}g foreach @all_bleats;

    	$global_all_users{"$user_to_show"}{"all_bleats"} = [ @all_bleats ]; # Hashes of Hashes
	    
	    # Determine the Max Bleat ID for a user & Update necessarily
    	my ($max_user_bleat_id) = (sort {$a <=> $b} @all_bleats)[-1];
    	if ($latest_bleat_id < $max_user_bleat_id){
    		$latest_bleat_id = $max_user_bleat_id;
			$global_bitter{"latest_bleat_id"} = $latest_bleat_id;
    	}

	    close $p;
	}
}





# Create a Database that gets All of the Users' Bleats
sub get_all_users_bleats{
	my @all_bleats = glob("$bleats_dir/*");

	for my $bleat_id (@all_bleats){
		my $details_filename = "$bleat_id";

		$bleat_id =~ s/$bleats_dir\///;
		$bleat_id = remove_new_line_and_spaces($bleat_id);
		$bleat_id = lc($bleat_id);

	    open my $p, "$details_filename" or die "Bitter cannot open $details_filename: $!";
	    
	    while (my $detail = <$p>) {
	    	$detail =~ /^(.*?):(.*)/;

	    	my $key = $1;	 	# Key of Hash
	    	$key = remove_new_line_and_spaces($key);

	    	my $value = $2;		# Value of Hash
	    	$value = remove_new_line_and_spaces($value);

	    	if ($key eq "bleat"){
    			my @mentions = $value =~ /\@([A-Za-z0-9\_\-\.]+)/gi;

				foreach my $mention (@mentions){
					$mention = lc($mention);

					if (defined $global_all_users{"$mention"}){
						my @user_mention = @{ $global_all_users{"$mention"}{"mention_bleat"} };
						push (@user_mention, "$bleat_id");
						$global_all_users{"$mention"}{"mention_bleat"} = [ @user_mention ]; # Hashes of Hashes
					}
				}
	    	}

	    	$global_all_user_bleats{"$bleat_id"}{"$key"} = "$value"; # Hashes of Hashes
	    }

	    close $p;
	}
}





# Get User Profile and Display It through HTML
sub get_user_profile{
	my $user = $_[0] || "";
	my $page_number = $_[1] || 1;

	$user = remove_new_line_and_spaces($user);
	$user = lc($user);

	my $is_personal = is_follow_display("$user");

	if (defined $global_all_users{"$user"}){
		my %user_details = %{$global_all_users{"$user"}};

		my $status		= $user_details{"status"};
		my $logged_in_user = get_currently_logged_in_user();

		if ($status eq "suspend" && "$logged_in_user" ne "$user"){
			HTML::header("Bitter - User not Found");
			HTML::navigation();
			HTML::profile_not_found("$user");
			HTML::footer();
		}
		else{
			my $username 		= $user_details{"username"};
			my $fullname 		= $user_details{"full_name"};
			my $profile_image 	= $user_details{"profile_pic"};
			my @listen_to 		= @{ $user_details{"listens"} }; # Hashes of Hashes
			my $latitude		= $user_details{"home_latitude"};
			my $longitude		= $user_details{"home_longitude"};
			my $suburb			= $user_details{"home_suburb"};
			my @all_bleats 		= @{ $user_details{"all_bleats"} }; 
			my $prof_text		= $user_details{"profile_text"};

			my @temp_listen_to = ();
			foreach my $listening (@listen_to){
				$listening = lc($listening);

				my $status_listening = $global_all_users{"$listening"}{"status"};

				if ($status_listening ne "suspend"){
					push(@temp_listen_to, $listening);
				}
			}

			if (not defined $profile_image){
				$profile_image = $no_profile;
			}
			if (not defined $latitude){
				$latitude = "";
			}
			if (not defined $longitude){
				$longitude = "";
			}
			if (not defined $suburb){
				$suburb = "";
			}
				
			my $listening_sum = @temp_listen_to;
			my $bleats_sum = @all_bleats;
			my $is_follow = is_currrently_following("$username");

			HTML::header("Bitter - \@$username");
			HTML::navigation("Profile");

			HTML::build_profile($username, $fullname, $profile_image, $listening_sum, $latitude, $longitude, $suburb, $bleats_sum, $is_personal, $is_follow, $prof_text);

			get_user_bleats("$username", "$page_number");

			if ($is_follow){
				HTML::footer(HTML::build_follow_hover_script(),
					HTML::build_profile_toggle_script());
			}
			else{
				HTML::footer(HTML::build_profile_toggle_script());
			}
		}
	}
	else{
		# If a user is not foumd, it will show profile not found page.
		HTML::header("Bitter - User not Found");
		HTML::navigation();
		HTML::profile_not_found("$user");
		HTML::footer();
	}
}





# Get User Bleats and Display It through HTML after getting User Profile.
sub get_user_bleats {
	my $username = $_[0] || "";
	my $page_number = $_[1] || 1;
	$page_number = $page_number - 1;

	$username = remove_new_line_and_spaces($username);
	$username = lc($username);

	$page_number =~ s/\Q\D\E//g;
	$page_number = 0 if (length($page_number) == 0 || $page_number < 0);


	my $fullname = $global_all_users{"$username"}{"full_name"};
	my $profile_image = $global_all_users{"$username"}{"profile_pic"};

	my @sorted_bleats = get_user_bleats_helper($username);

	my $total_page_number = roundup(scalar(@sorted_bleats) / $result_limit);
	my @sorted_bleats_limit = ();

	if (scalar(@sorted_bleats) > 0){
		@sorted_bleats_limit = @sorted_bleats[($result_limit * $page_number) .. (($result_limit * ($page_number + 1)) - 1)];

		if ($page_number > ($total_page_number - 1)){
			$page_number = 0;
			@sorted_bleats_limit = @sorted_bleats[($result_limit * $page_number) .. (($result_limit * ($page_number + 1)) - 1)];
		}
	}


	if (scalar(@sorted_bleats_limit) > 0){
		HTML::build_bleats_start();

		foreach my $sorted_bleat_id (@sorted_bleats_limit){
			if (defined $sorted_bleat_id){
				my $username 	= $global_all_user_bleats{"$sorted_bleat_id"}{'username'};
				my $bleat 		= $global_all_user_bleats{"$sorted_bleat_id"}{'bleat'};
				my $latitude 	= $global_all_user_bleats{"$sorted_bleat_id"}{'latitude'};
				my $longitude 	= $global_all_user_bleats{"$sorted_bleat_id"}{'longitude'};
				my $time 		= $global_all_user_bleats{"$sorted_bleat_id"}{'time'};
				my $in_reply 	= $global_all_user_bleats{"$sorted_bleat_id"}{'in_reply_to'};

				my $is_personal = is_follow_display("$username");
				my $is_follow = is_currrently_following("$username");

				my ($file_one, $file_two, $file_three, $file_four) = get_upload_file_location($sorted_bleat_id);	

				HTML::help_build_individual_bleat($sorted_bleat_id, $username, $fullname, $profile_image, 
					$bleat, $time, $latitude, $longitude, $in_reply, $is_personal, $is_follow, 0, $file_one, $file_two, $file_three, $file_four);
			}
		}

		HTML::build_bleats_end();	
		get_user_listening("$username");

	    HTML::build_pagination_bar("profile", "$page_number", "$result_limit", "$total_page_number", "$pagination_limit");
	}
	else{
		HTML::build_no_bleat_profile();
		get_user_listening("$username");
		HTML::build_pagination_bar("profile", "$page_number", "$result_limit", "$total_page_number", "$pagination_limit");
	}
}




# This function gets the list of following of a particular user.
sub get_user_listening {
	my $username = $_[0] || "";

	$username = remove_new_line_and_spaces($username);
	$username = lc($username);

	my @listening_list = @{ $global_all_users{"$username"}{"listens"} }; 

	if (scalar(@listening_list) > 0){
		HTML::build_listening_start();

		foreach my $listening (@listening_list){
			if (defined $listening){
				$listening = lc($listening);

				my $username_svr 	= $global_all_users{"$listening"}{"username"};
				my $fullname_svr 	= $global_all_users{"$listening"}{"full_name"};
				my $prof_img_svr 	= $global_all_users{"$listening"}{"profile_pic"};
				my @listen_to 		= @{ $global_all_users{"$listening"}{"listens"} }; # Hashes of Hashes

				my $listening_sum = @listen_to;
				my $is_personal = is_follow_display("$username_svr");
				my $is_follow = is_currrently_following("$username_svr");

				HTML::build_individual_listening($username_svr, $fullname_svr, $prof_img_svr, $listening_sum, $is_personal, $is_follow);
			}
		}

		HTML::build_listening_end();
	}
	else{
		HTML::build_no_listening();
	}
}




# Helper method to get a particular user's bleats in sorted manner (Portability as it can take multiple user simultenaously)
sub get_user_bleats_helper{
	my @users_passed_in = @_;

	my %individual_bleat = ();
	my @sorted_bleats = ();

	foreach my $username (@users_passed_in){
		$username = remove_new_line_and_spaces($username);
		$username = lc($username);

		my @all_bleats 	= @{ $global_all_users{"$username"}{"all_bleats"} }; # Hashes of Hashes

		foreach my $bleat_id (@all_bleats){
			$bleat_id = remove_new_line_and_spaces($bleat_id); # There is new line in the Bleat ID
			$bleat_id = lc($bleat_id);

			$individual_bleat{"$bleat_id"} = $global_all_user_bleats{"$bleat_id"};
		}
	}

	@sorted_bleats = sort { $individual_bleat{$b}{'time'} <=> $individual_bleat{$a}{'time'} } keys %individual_bleat;

	return @sorted_bleats;
}





# This function is used to check whether the password matches the password in the databse given a username.
sub check_login_id_password {
	my $login_id = $_[0];
	my $password = $_[1];

	$login_id =~ s/[^A-Za-z0-9\_\-\.\@]//g;
	$login_id = remove_new_line_and_spaces($login_id);
	$login_id = lc($login_id);

	$password = remove_new_line_and_spaces($password);
	$password = Security::encode_data_md5($password);

	foreach my $user (sort keys %global_all_users){
		my $username_svr 	= $global_all_users{"$user"}{"username"};
		my $email_svr 		= $global_all_users{"$user"}{"email"};

		if (lc("$username_svr") eq "$login_id" || lc("$email_svr") eq "$login_id"){
			my $password_svr = $global_all_users{"$user"}{"password"};

			if ("$password_svr" eq "$password"){
				my $account_status  = $global_all_users{"$user"}{"status"};

				if ("$account_status" eq "valid"){
					return 1; # Sucess Validation
				}
				elsif ("$account_status" eq "verify"){
					return 2; # Require Verification
				}
				elsif ("$account_status" eq "suspend"){
					return 3; # Suspended Account
				}
				else{
					return 0;
				}
			}
			else{
				return 0; # Fail Validation
			}
		}
	}

	return 0; # Fail Validation
}





# This function helps to get the username of the user given that an email is used for log in.
sub get_username_by_email{
	my $login_id = $_[0];
	$login_id =~ s/[^A-Za-z0-9\_\-\.\@]//g;

	$login_id = remove_new_line_and_spaces($login_id);
	$login_id = lc($login_id);

    if($login_id =~ /^[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,}$/) {
    	foreach my $user (sort keys %global_all_users){
			my $email_svr 		= $global_all_users{"$user"}{"email"};
			my $username_svr 	= $global_all_users{"$user"}{"username"};

			if (lc($email_svr) =~ /$login_id/i){
				return lc("$username_svr");
			}
		}

		return "";
    }
    else{
    	return lc("$login_id");
    }
}





# This function helsp to check whether the username and email is valid and exists.
sub check_username_email{
	my $login_id = $_[0];
	$login_id =~ s/[^A-Za-z0-9\_\-\.\@]//g;

	$login_id = remove_new_line_and_spaces($login_id);
	$login_id = lc($login_id);

	if (defined $global_all_users{"$login_id"}){
		return 1;
	}
	elsif ($login_id =~ /^[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,}$/) {
    	foreach my $user (sort keys %global_all_users){
			my $email_svr 		= $global_all_users{"$user"}{"email"};
			my $username_svr 	= $global_all_users{"$user"}{"username"};

			if (lc($email_svr) =~ /$login_id/i){
				return 1;
			}
		}
    }

	return 0;
}





# This function helsp to search for username, fullname or even bleats
sub search_username_fullname_bleat{
	my $user = $_[0] || "";
	my $search_value = $_[1] || "";
	my $search_option = $_[2] || "";

	my $page_number = $_[2] || 1;
	$page_number = $page_number - 1;

	$search_value = remove_new_line_and_spaces($search_value);
	$search_option = remove_new_line_and_spaces($search_option);

	$search_option = lc($search_option);

	$page_number =~ s/\Q\D\E//g;
	$page_number = 0 if (length($page_number) == 0 || $page_number < 0);

	my @found_result = ();
	my $logged_in_user = get_currently_logged_in_user();

	if (length($search_value) > 0){
		HTML::header("Search \: $search_value");
		HTML::navigation("Search");

		if ($search_option eq "bleat"){
			my @temp_hashtag = ();
			my @temp_other = ();

			my %temp_bleat_hashtag = ();
			my %temp_bleat_other = ();

			my $no_hashtag_search = $search_value;
			$no_hashtag_search =~ s/^\#?//;

			foreach my $bleat_id (sort keys %global_all_user_bleats){

				my $bleat_message = $global_all_user_bleats{"$bleat_id"}{"bleat"};
				my @found_hashtag = $bleat_message =~ /[^\:\&]?\#(\w+)/gi;

				if (grep(/^$no_hashtag_search$/gi, @found_hashtag)) { # Check if there is matching hashtags
					push (@temp_hashtag, $bleat_id);
				}
				elsif ($bleat_message =~ /\Q$search_value\E/i){
					if (! grep(/^$bleat_id$/i, @temp_hashtag)) {	
						push (@temp_other, $bleat_id);
					}
				}
			}



			foreach my $bleat_id (@temp_hashtag){
				$bleat_id = remove_new_line_and_spaces($bleat_id);  # There is new line in the Bleat ID
				$bleat_id = lc($bleat_id);

				my $bleat_user = $global_all_user_bleats{"$bleat_id"}{"username"};
				$bleat_user = lc($bleat_user);

				if ("$bleat_user" ne "$user"){
					my $status = $global_all_users{"$bleat_user"}{"status"};

					if ($status ne "suspend"){
						$temp_bleat_other{"$bleat_id"} = $global_all_user_bleats{"$bleat_id"};
					}
				}
				else{
					$temp_bleat_other{"$bleat_id"} = $global_all_user_bleats{"$bleat_id"};
				}
			}

			foreach my $bleat_id (@temp_other){
				$bleat_id = remove_new_line_and_spaces($bleat_id);  # There is new line in the Bleat ID
				$bleat_id = lc($bleat_id);

				my $bleat_user = $global_all_user_bleats{"$bleat_id"}{"username"};
				$bleat_user = lc($bleat_user);

				if ("$bleat_user" ne "$user"){
					my $status = $global_all_users{"$bleat_user"}{"status"};

					if ($status ne "suspend"){
						$temp_bleat_other{"$bleat_id"} = $global_all_user_bleats{"$bleat_id"};
					}
				}	
				else{
					$temp_bleat_other{"$bleat_id"} = $global_all_user_bleats{"$bleat_id"};
				}			
			}

			@temp_hashtag = sort { $temp_bleat_hashtag{$b}{'time'} <=> $temp_bleat_hashtag{$a}{'time'} } keys %temp_bleat_hashtag;
			@temp_other = sort { $temp_bleat_other{$b}{'time'} <=> $temp_bleat_other{$a}{'time'} } keys %temp_bleat_other;

			push(@found_result, @temp_hashtag);
			push(@found_result, @temp_other);
		}
		else{
			foreach my $user_ind (sort keys %global_all_users){
				my $username_svr 	= $global_all_users{"$user_ind"}{"username"};
				my $fullname_svr 	= $global_all_users{"$user_ind"}{"full_name"};
				my $status 			= $global_all_users{"$user_ind"}{"status"};

				if (lc("$username_svr") =~ /\Q$search_value\E/i || lc("$fullname_svr") =~ /\Q$search_value\E/i){
					push(@found_result, lc($username_svr)) if ($status ne "suspend" || "$logged_in_user" eq "$user_ind");
				}
			}
		}
	}
	else{
		HTML::header("Bitter - Search");
		HTML::navigation("Search");
	}

	HTML::build_search_page("$search_value", $search_option);

	if (length($search_value) > 0){
		HTML::build_search_start();

		my $total_page_number = roundup(scalar(@found_result) / $result_limit);
		my @found_result_limit = ();

		if (scalar(@found_result) > 0){
			@found_result_limit = @found_result[($result_limit * $page_number) .. (($result_limit * ($page_number + 1)) - 1)];

			if ($page_number > ($total_page_number - 1)){
				$page_number = 0;
				@found_result_limit = @found_result[($result_limit * $page_number) .. (($result_limit * ($page_number + 1)) - 1)];
			}
		}

		if (scalar(@found_result_limit) > 0){
			if ($search_option eq "bleat"){
				foreach my $sorted_bleat_id (@found_result_limit){
					if (defined $sorted_bleat_id){
						my $username 	= $global_all_user_bleats{"$sorted_bleat_id"}{'username'};
						my $bleat 		= $global_all_user_bleats{"$sorted_bleat_id"}{'bleat'};
						my $latitude 	= $global_all_user_bleats{"$sorted_bleat_id"}{'latitude'};
						my $longitude 	= $global_all_user_bleats{"$sorted_bleat_id"}{'longitude'};
						my $time 		= $global_all_user_bleats{"$sorted_bleat_id"}{'time'};
						my $in_reply 	= $global_all_user_bleats{"$sorted_bleat_id"}{'in_reply_to'};

						my $fullname = $global_all_users{lc("$username")}{"full_name"};
						my $profile_image = $global_all_users{lc("$username")}{"profile_pic"};

						my $is_personal = is_follow_display("$username");
						my $is_follow = is_currrently_following("$username");

						my ($file_one, $file_two, $file_three, $file_four) = get_upload_file_location($sorted_bleat_id);				

					    HTML::help_build_individual_bleat($sorted_bleat_id, $username, $fullname, $profile_image, 
					    	$bleat, $time, $latitude, $longitude, $in_reply, $is_personal, $is_follow, 1, $file_one, $file_two, $file_three, $file_four);
					}
				}
			}
			else{
				foreach my $user (@found_result_limit){
					if (defined $user){
						my $username_svr 	= $global_all_users{"$user"}{"username"};
						my $fullname_svr 	= $global_all_users{"$user"}{"full_name"};
						my $prof_img_svr 	= $global_all_users{"$user"}{"profile_pic"};
						my @listen_to 		= @{ $global_all_users{"$user"}{"listens"} }; # Hashes of Hashes

						my $listening_sum = @listen_to;
						my $is_personal = is_follow_display("$username_svr");
						my $is_follow = is_currrently_following("$username_svr");

						HTML::build_individual_search($search_value, $search_option, $username_svr, 
							$fullname_svr, $prof_img_svr, $listening_sum, $is_personal, $is_follow);
					}		
				}
			}
		}
		else{
			HTML::build_not_found_search();
		}

		HTML::build_search_end();

		if (scalar(@found_result_limit) > 0){
		    HTML::build_pagination_bar("search", "$page_number", "$result_limit", "$total_page_number", "$pagination_limit", "q=$search_value&qo=$search_option");
		}
	}
	
	HTML::footer(HTML::build_check_search_script());
}





# This function helps user to create a new bleat
sub new_bleat{
	my $username = $_[0];
	my $bleat_message = $_[1]; # Bleat message
    my $bleat_latitude = $_[2] || ""; # Bleat Latitude
	my $bleat_longitude = $_[3] || ""; # Bleat Longitude
	my $reply_to_bleat = $_[4] || "";


	$username = remove_new_line_and_spaces($username);
	$username = lc($username);

	$bleat_latitude = remove_new_line_and_spaces($bleat_latitude);
	$bleat_longitude = remove_new_line_and_spaces($bleat_longitude);
	$reply_to_bleat = remove_new_line_and_spaces($reply_to_bleat);
	$bleat_message = remove_new_line_and_spaces($bleat_message);


	my $time = time; # Get the epoch current time

	if (length($bleat_message) == 0){
		return 0;
	}
	else{
		my @all_bleats 	= @{ $global_all_users{lc("$username")}{"all_bleats"} }; # Hashes of Hashes

		# Prevent Duplicate Bleats from being made
		foreach my $temp_bleat_id (@all_bleats){
			my $message = $global_all_user_bleats{"$temp_bleat_id"}{"bleat"};
			$message = remove_new_line_and_spaces($message);
			$message = lc($message);

			if ("$message" eq lc("$bleat_message")){
				return 0;
			}
		}

		my $new_bleat_id = $latest_bleat_id++;
		my $bleat_content = "";

		$bleat_message =~ s/\n/\<br\/\>/g;

		while (-f "$bleats_dir/$new_bleat_id"){ # Make sure that the bleat id does not clashes
			$new_bleat_id++;
		}

		# Necessasry
		open(my $bleat_file_temp,  ">", "$bleats_dir/$new_bleat_id") or return 0; # Update Bleats file in Bleats Folder
		print $bleat_file_temp " ";
		close $bleat_file_temp;

		$global_bitter{"latest_bleat_id"} = $new_bleat_id;

		my $display_username = $global_all_users{"$username"}{"username"};

		$bleat_content .= "username: $display_username" . "\n";
		$bleat_content .= "time: $time" . "\n";
		$bleat_content .= "bleat: $bleat_message" . "\n";

		$global_all_user_bleats{"$new_bleat_id"}{"username"} = $display_username;
		$global_all_user_bleats{"$new_bleat_id"}{"time"} = $time;
		$global_all_user_bleats{"$new_bleat_id"}{"bleat"} = $bleat_message;


		if (length($bleat_latitude) > 0){
			$bleat_content .= "latitude: $bleat_latitude" . "\n";
			$global_all_user_bleats{"$new_bleat_id"}{"latitude"} = $bleat_latitude;
		}

		if (length($bleat_longitude) > 0){
			$bleat_content .= "longitude: $bleat_longitude" . "\n";
			$global_all_user_bleats{"$new_bleat_id"}{"longitude"} = $bleat_longitude;
		}

		if (length($reply_to_bleat) > 0){
			if (defined $global_all_user_bleats{lc("$reply_to_bleat")}){ # If bleats does not exists, cannot add as a reply to bleat
				$bleat_content .= "in_reply_to: $reply_to_bleat" . "\n";
				$global_all_user_bleats{"$new_bleat_id"}{"in_reply_to"} = $reply_to_bleat;

				my $mentionee = $global_all_user_bleats{"$reply_to_bleat"}{"username"};
				my $old_msg = $global_all_user_bleats{"$reply_to_bleat"}{"bleat"};

				notification_reply("$mentionee", "$username", "$bleat_message");
			}
		}

		push (@all_bleats, $new_bleat_id);
	    @all_bleats = reverse sort @all_bleats;
    	$global_all_users{lc("$username")}{"all_bleats"} = [ @all_bleats ]; # Hashes of Hashes


    	# Update Mentions of the user (attach email code here)
		my @mentions = $bleat_message =~ /\@([A-Za-z0-9\_\-\.]+)/gi;
		foreach my $mention (@mentions){
			$mention = lc($mention);

			if (defined $global_all_users{"$mention"}){
				my @user_mention = @{ $global_all_users{"$mention"}{"mention_bleat"} };
				push (@user_mention, "$new_bleat_id");
				$global_all_users{"$mention"}{"mention_bleat"} = [ @user_mention ]; # Hashes of Hashes

				notification_mentioned("$mention", "$username", "$bleat_message"); # Notify Email when Mentioned
			}
		}


    	my $folder_name = $global_all_users{"$username"}{"username"};
		open(my $bleat_txt,  ">", "$users_dir/$folder_name/bleats.txt") or return 0; # Update User Bleats in Bleats.txt
		print $bleat_txt join("\n", @all_bleats);
		close $bleat_txt;


		open(my $bleat_file,  ">", "$bleats_dir/$new_bleat_id") or return 0; # Update Bleats file in Bleats Folder
		print $bleat_file $bleat_content;
		close $bleat_file;

		update_database(1) or return 0;
		update_database(2) or return 0;
		update_database(3) or return 0;

		build_database("no");

		return $new_bleat_id;
	}
}





# This function helps to get all of the relevant bleats (mentioned, following and user's own bleat) and display it 
# in a nice ly formatted HTML.
sub all_relevant_bleats {
	my $username_original = $_[0] || "";
	my $page_number = $_[1] || 1;
	$page_number = $page_number - 1;

	$username_original = remove_new_line_and_spaces($username_original);
	$username_original = lc($username_original);

	$page_number =~ s/\Q\D\E//g;
	$page_number = 0 if (length($page_number) == 0 || $page_number < 0);


	my @sorted_bleats = get_user_relevant_bleat_helper("$username_original");

	my $total_page_number = roundup(scalar(@sorted_bleats) / $result_limit);
	my @sorted_bleats_limit = ();

	if (scalar(@sorted_bleats) > 0){
		@sorted_bleats_limit = @sorted_bleats[($result_limit * $page_number) .. (($result_limit * ($page_number + 1)) - 1)];

		if ($page_number > ($total_page_number - 1)){
			$page_number = 0;
			@sorted_bleats_limit = @sorted_bleats[($result_limit * $page_number) .. (($result_limit * ($page_number + 1)) - 1)];
		}
	}
	
	if (scalar(@sorted_bleats_limit) > 0){
		HTML::build_home_bleats_start();

		foreach my $sorted_bleat_id (@sorted_bleats_limit){
			if (defined $sorted_bleat_id){	
				my $username 	= $global_all_user_bleats{"$sorted_bleat_id"}{'username'};
				my $bleat 		= $global_all_user_bleats{"$sorted_bleat_id"}{'bleat'};
				my $latitude 	= $global_all_user_bleats{"$sorted_bleat_id"}{'latitude'};
				my $longitude 	= $global_all_user_bleats{"$sorted_bleat_id"}{'longitude'};
				my $time 		= $global_all_user_bleats{"$sorted_bleat_id"}{'time'};
				my $in_reply 	= $global_all_user_bleats{"$sorted_bleat_id"}{'in_reply_to'};

				my $fullname = $global_all_users{lc("$username")}{"full_name"};
				my $profile_image = $global_all_users{lc("$username")}{"profile_pic"};

				my $is_personal = is_follow_display("$username");
				my $is_follow = is_currrently_following("$username");

				my ($file_one, $file_two, $file_three, $file_four) = get_upload_file_location($sorted_bleat_id);				

			    HTML::help_build_individual_bleat($sorted_bleat_id, $username, $fullname, $profile_image, 
			    	$bleat, $time, $latitude, $longitude, $in_reply, $is_personal, $is_follow, 1, $file_one, $file_two, $file_three, $file_four);
			}
		}

		HTML::build_home_bleats_end();
		HTML::build_pagination_bar("home", "$page_number", "$result_limit", $total_page_number, $pagination_limit);
	}
	else{
		HTML::build_no_bleat();
	}
}





# This function acts as a helper to get relevant bleats.
sub get_user_relevant_bleat_helper{
	my $username = $_[0];
	$username = remove_new_line_and_spaces($username);
	$username = lc($username);

	my %individual_bleat = ();
	my @sorted_bleats = ();

	my @all_bleats 			= @{ $global_all_users{"$username"}{"all_bleats"} }; # Hashes of Hashes
	my @all_bleats_mention 	= @{ $global_all_users{"$username"}{"mention_bleat"} }; # Hashes of Hashes
	my @all_listen_to		= @{ $global_all_users{"$username"}{"listens"} };

	foreach my $mention (@all_bleats_mention){
		my $user_mention = $global_all_user_bleats{"$mention"}{"username"};
		$user_mention = lc($user_mention);

		if (grep(/^$user_mention$/i, @all_listen_to)) {	# If user does not follow anymore a specific user, should not display in the home screen
			push(@all_bleats, $mention);
		}		
	}
	
	foreach my $bleat_id (@all_bleats){
		$bleat_id = remove_new_line_and_spaces($bleat_id); # There is new line in the Bleat ID
		$bleat_id = lc($bleat_id);

		$individual_bleat{"$bleat_id"} = $global_all_user_bleats{"$bleat_id"};
	}

	foreach my $listen_to (@all_listen_to){
		$listen_to = lc($listen_to);
		$listen_to = remove_new_line_and_spaces($listen_to);

		my @all_bleats_listen 	= @{ $global_all_users{"$listen_to"}{"all_bleats"} }; # Hashes of Hashes

		foreach my $bleat_id (@all_bleats_listen){
			$bleat_id = remove_new_line_and_spaces($bleat_id); # There is new line in the Bleat ID
			$bleat_id = lc($bleat_id);

			$individual_bleat{"$bleat_id"} = $global_all_user_bleats{"$bleat_id"};
		}
	}
	
	foreach my $bleat_id (keys %individual_bleat){
		my $bleat_user = $global_all_user_bleats{"$bleat_id"}{"username"};
		$bleat_user = lc($bleat_user);

		if ("$bleat_user" ne "$username"){
			my $status = $global_all_users{"$bleat_user"}{"status"};

	    	delete $individual_bleat{"$bleat_id"} if ($status eq "suspend");
		}
	}

	@sorted_bleats = sort { $individual_bleat{$b}{'time'} <=> $individual_bleat{$a}{'time'} } keys %individual_bleat;

	return @sorted_bleats;
}





# This function helps to get all bleats that mention the currently logged in user.
sub all_mentions{
	my $username_original = $_[0] || "";
	my $page_number = $_[1] || 1;
	$page_number = $page_number - 1;


	$username_original = remove_new_line_and_spaces($username_original);
	$username_original = lc($username_original);

	$page_number =~ s/\Q\D\E//g;
	$page_number = 0 if (length($page_number) == 0 || $page_number < 0);
	

	my %individual_bleat = ();
	my @all_bleats_mention 	= @{ $global_all_users{"$username_original"}{"mention_bleat"} }; # Hashes of Hashes


	foreach my $bleat_id (@all_bleats_mention){
		$bleat_id = remove_new_line_and_spaces($bleat_id);  # There is new line in the Bleat ID
		$bleat_id = lc($bleat_id);

		my $bleat_user = $global_all_user_bleats{"$bleat_id"}{"username"};
		$bleat_user = lc($bleat_user);

		if ("$bleat_user" ne "$username_original"){
			my $status = $global_all_users{"$bleat_user"}{"status"};

			if ($status ne "suspend"){
				$individual_bleat{"$bleat_id"} = $global_all_user_bleats{"$bleat_id"};
			}
		}
		else {
			$individual_bleat{"$bleat_id"} = $global_all_user_bleats{"$bleat_id"};
		}
	}


	@all_bleats_mention = sort { $individual_bleat{$b}{'time'} <=> $individual_bleat{$a}{'time'} } keys %individual_bleat;
	

	my $total_page_number = roundup(scalar(@all_bleats_mention) / $result_limit);
	my @all_bleats_mention_limit = ();

	if (scalar(@all_bleats_mention) > 0){
		@all_bleats_mention_limit = @all_bleats_mention[($result_limit * $page_number) .. (($result_limit * ($page_number + 1)) - 1)];

		if ($page_number > ($total_page_number - 1)){
			$page_number = 0;
			@all_bleats_mention_limit = @all_bleats_mention[($result_limit * $page_number) .. (($result_limit * ($page_number + 1)) - 1)];
		}
	}


	if (scalar(@all_bleats_mention_limit) > 0){
		HTML::build_home_bleats_start();
	
		foreach my $sorted_bleat_id (@all_bleats_mention_limit){
			if (defined $sorted_bleat_id){
				my $username 	= $global_all_user_bleats{"$sorted_bleat_id"}{'username'};
				my $bleat 		= $global_all_user_bleats{"$sorted_bleat_id"}{'bleat'};
				my $latitude 	= $global_all_user_bleats{"$sorted_bleat_id"}{'latitude'};
				my $longitude 	= $global_all_user_bleats{"$sorted_bleat_id"}{'longitude'};
				my $time 		= $global_all_user_bleats{"$sorted_bleat_id"}{'time'};
				my $in_reply 	= $global_all_user_bleats{"$sorted_bleat_id"}{'in_reply_to'};

				my $fullname = $global_all_users{lc("$username")}{"full_name"};
				my $profile_image = $global_all_users{lc("$username")}{"profile_pic"};

				my $is_personal = is_follow_display("$username");
				my $is_follow = is_currrently_following("$username");

				my ($file_one, $file_two, $file_three, $file_four) = get_upload_file_location($sorted_bleat_id);				

			    HTML::help_build_individual_bleat($sorted_bleat_id, $username, $fullname, $profile_image, 
			    	$bleat, $time, $latitude, $longitude, $in_reply, $is_personal, $is_follow, 1, $file_one, $file_two, $file_three, $file_four);
			}
		}

		HTML::build_home_bleats_end();
	    HTML::build_pagination_bar("mentions", "$page_number", "$result_limit", "$total_page_number", "$pagination_limit");
	}
	else{
		HTML::build_no_bleat();
	}
}





# This function helps to add or remove a following of a user. (This means that the user can listen / unlisten another user)
sub add_remove_following {
	my $username_original = $_[0] || "";
	my $username_add_remove = $_[1] || "";
	my $should_add = $_[2] || 0;

	$username_original = remove_new_line_and_spaces($username_original);
	$username_original = lc($username_original);

	$username_add_remove = remove_new_line_and_spaces($username_add_remove);
	$username_add_remove = lc($username_add_remove);

	if ("$username_original" eq "$username_add_remove"){
		return 0;
	}

	if (not defined $global_all_users{lc("$username_add_remove")}){
		return 0;
	}

	my @listen_to_profile = @{ $global_all_users{lc("$username_original")}{"listens"} }; # Hashes of Hashes
	
	if ($should_add){
		push(@listen_to_profile, $username_add_remove);
		notification_being_listened("$username_add_remove", "$username_original");
	}
	else{
		@listen_to_profile = grep {!/^$username_add_remove$/} @listen_to_profile;
	}

	$global_all_users{lc("$username_original")}{"listens"} = [ @listen_to_profile ];

	my $listens_value = join(" ", @listen_to_profile);

	update_detail_file($username_original, "listens", $listens_value) or return 0;

	update_database(1) or return 0;
	build_database("no");

	return 1;
}





# This function helps to register a new user.
sub register_new_user {
	my $username = $_[0];
	my $email = $_[1] || "";
	my $password = $_[2] || "";

	my $full_name = $_[3] || "";
	my $latitude = $_[4] || "";
	my $longitude = $_[5] || "";
	my $suburb = $_[6] || "";

	my $url_link = $_[7] || "";

	my $lower_cased_username = lc("$username");
	my $lower_cased_email = lc("$email");

	foreach my $user (sort keys %global_all_users){
		my $username_svr 	= $global_all_users{"$user"}{"username"};
		my $email_svr 		= $global_all_users{"$user"}{"email"};

		if (lc("$username_svr") eq "$lower_cased_username"){
			return 2;
		}
		elsif (lc("$email_svr") eq "$lower_cased_email"){
			return 3;
		}
	}

	if (not defined $global_all_users{"$lower_cased_username"}){
		my $make_detail = "";

		$make_detail .= "username: $username\n";
		$make_detail .= "email: $email\n";
		$make_detail .= "password: $password\n";
		$make_detail .= "listens: \n";

		$global_all_users{"$lower_cased_username"}{"username"} = $username;
		$global_all_users{"$lower_cased_username"}{"email"} = $email;
		$global_all_users{"$lower_cased_username"}{"password"} = Security::encode_data_md5($password);

		my @all_listens = ();
    	$global_all_users{"$lower_cased_username"}{"listens"} = [ @all_listens ]; # Hashes of Hashes

		if (length($full_name) > 0){
			$make_detail .= "full_name: $full_name" . "\n";
			$global_all_users{"$lower_cased_username"}{"full_name"} = $full_name;
		}

		if (length($latitude) > 0){
			$make_detail .= "home_latitude: $latitude" . "\n";
			$global_all_users{"$lower_cased_username"}{"home_latitude"} = $latitude;
		}

		if (length($longitude) > 0){
			$make_detail .= "home_longitude: $longitude" . "\n";
			$global_all_users{"$lower_cased_username"}{"home_longitude"} = $longitude;
		}

		if (length($suburb) > 0){
			$make_detail .= "home_suburb: $suburb" . "\n";
			$global_all_users{"$lower_cased_username"}{"home_suburb"} = $suburb;
		}


		my @all_bleats = ();
    	$global_all_users{"$lower_cased_username"}{"all_bleats"} = [ @all_bleats ]; # Hashes of Hashes

	    my @mention_array = ();
	    $global_all_users{"$lower_cased_username"}{"mention_bleat"} = [ @mention_array ]; # Hashes of Hashes


    	$global_all_users{"$lower_cased_username"}{"profile_pic"} = $no_profile; # Hashes of Hashes
	    $global_all_users{"$lower_cased_username"}{"background_pic"} = $default_background; # Hashes of Hashes
	    $global_all_users{"$lower_cased_username"}{"status"} = "verify"; # Status : Verify (Sign Up Verification Required), Valid (Normal), Suspend (Invincible Account)
	    $global_all_users{"$lower_cased_username"}{"profile_text"} = ""; # Profile Text


	    $global_all_users{"$username"}{"notify_bleat"} = "off"; # Should notify when someone mentioned in a bleat
	    $global_all_users{"$username"}{"notify_reply"} = "off"; # Should notify when someone reply user's bleat
	    $global_all_users{"$username"}{"notify_new_listener"} = "off"; # Should notify when gain new listener


	    my $activation_code = Security::encode_data_md5("$lower_cased_username|" . time . "|$password");
	    $global_all_users{"$username"}{"activation_code"} = $activation_code; # Set Verifcation (Activation Code) for email purpose


	    $global_all_users{"$username"}{"recover_password_code"} = "";


	    my $folder_name = $global_all_users{"$username"}{"username"};
	    mkdir "$users_dir/$folder_name";

		open(my $details_txt,  ">", "$users_dir/$folder_name/details.txt") or return 2; # Create User Details in Details.txt
		print $details_txt $make_detail;
		close $details_txt;

		open(my $bleats_txt,  ">", "$users_dir/$folder_name/bleats.txt") or return 2; # Create User Bleats in Bleats.txt
		print $bleats_txt " ";
		close $bleats_txt;

		update_database(1) or return 2;
		update_database(2) or return 2;
		update_database(3) or return 2;

		build_database("no");

		my $message_email = <<email;
			Hey, $username\,
			\n
			We're ready to activate your account. All we need to do is make sure this is your email address.
			\n
			Please activate your account by clicking the link below.
			\n
			$url_link?user=$lower_cased_username&activate=$activation_code
			\n
			\n
			If you didn't create a Bitter account, just delete this email and everything will go back to the way it was.
email

		send_email("$email", "Bitter Account Confirmation", "$message_email") or return 2;

		return 1;
	}
	else{
		return 2;
	}
}





# This function helps to activate a user's account using a given activation code
sub activate_user{
    my $activate_code = $_[0] || "";
    my $activate_user = $_[1] || "";

    $activate_user = lc($activate_user);

	if (defined $global_all_users{"$activate_user"}){
		my $account_status  = $global_all_users{"$activate_user"}{"status"};
		my $activation_code  = $global_all_users{"$activate_user"}{"activation_code"};

		if ("$account_status" eq "valid"){
			return 1; # Activated User
		}
		elsif ("$account_status" eq "verify"){

			if ("$activation_code" eq "$activate_code"){
				$global_all_users{"$activate_user"}{"activation_code"} = "";
				$global_all_users{"$activate_user"}{"status"} = "valid";

				update_database(1) or return 3;
				update_database(2) or return 3;
				update_database(3) or return 3;

				build_database("no");

				return 1;
			}
			else{
				return 2; # Failure	
			}
		}
		else{
			return 3; # General Failure
		}
	}
	else{
		return 0; # Non-existant user
	}
}




# This function helps to get the responses to a bleat (conversation between user's using bleats).
# It get's the bleats by checking the in_reply_to field in a bleat and determine whether there is a related bleat
# that is older and newer than the given bleat.
sub get_bleat_responses {
	my $bleat_id = $_[0] || "";
	my $page_number = $_[1] || 1;
	$page_number = $page_number - 1;

	$bleat_id = remove_new_line_and_spaces($bleat_id);
	$bleat_id = lc($bleat_id);

	$page_number =~ s/\Q\D\E//g;
	$page_number = 0 if (length($page_number) == 0 || $page_number < 0);


	if (defined $global_all_user_bleats{"$bleat_id"}){

		my @bleat_responses = ();
		push (@bleat_responses, "$bleat_id");

		my @recursive_down = recursive_down_response("$bleat_id");
		push (@bleat_responses, @recursive_down);

		my @recursive_up = recursive_up_response("$bleat_id");
		push (@bleat_responses, @recursive_up);


		my %individual_bleat = ();
		foreach my $bleat_id (@bleat_responses){
			$bleat_id = remove_new_line_and_spaces($bleat_id); # There is new line in the Bleat ID
			$bleat_id = lc($bleat_id);

			$individual_bleat{"$bleat_id"} = $global_all_user_bleats{"$bleat_id"};
		}
	
		@bleat_responses = sort { $individual_bleat{$a}{'time'} <=> $individual_bleat{$b}{'time'} } keys %individual_bleat;


		my $total_page_number = roundup(scalar(@bleat_responses) / $result_limit);
		my @bleat_responses_limit = ();

		if (scalar(@bleat_responses) > 0){
			@bleat_responses_limit = @bleat_responses[($result_limit * $page_number) .. (($result_limit * ($page_number + 1)) - 1)];

			if ($page_number > ($total_page_number - 1)){
				$page_number = 0;
				@bleat_responses_limit = @bleat_responses[($result_limit * $page_number) .. (($result_limit * ($page_number + 1)) - 1)];
			}
		}

		if (scalar(@bleat_responses_limit) > 0){
			HTML::build_conversation_bleat_start();
		
			foreach my $sorted_bleat_id (@bleat_responses_limit){
				if (defined $sorted_bleat_id){
					my $username 	= $global_all_user_bleats{"$sorted_bleat_id"}{'username'};
					my $bleat 		= $global_all_user_bleats{"$sorted_bleat_id"}{'bleat'};
					my $latitude 	= $global_all_user_bleats{"$sorted_bleat_id"}{'latitude'};
					my $longitude 	= $global_all_user_bleats{"$sorted_bleat_id"}{'longitude'};
					my $time 		= $global_all_user_bleats{"$sorted_bleat_id"}{'time'};
					my $in_reply 	= $global_all_user_bleats{"$sorted_bleat_id"}{'in_reply_to'};

					my $fullname = $global_all_users{lc("$username")}{"full_name"};
					my $profile_image = $global_all_users{lc("$username")}{"profile_pic"};

					my $is_personal = is_follow_display("$username");
					my $is_follow = is_currrently_following("$username");

					my ($file_one, $file_two, $file_three, $file_four) = get_upload_file_location($sorted_bleat_id);

					if ("$sorted_bleat_id" eq "$bleat_id"){
						HTML::build_home_bleats_end();

						HTML::build_conversation_bleat_start(1);

						HTML::help_build_individual_bleat($sorted_bleat_id, $username, $fullname, $profile_image, 
					    	$bleat, $time, $latitude, $longitude, $in_reply, $is_personal, $is_follow, 1, $file_one, $file_two, $file_three, $file_four);

						HTML::build_home_bleats_end();

						HTML::build_conversation_bleat_start(0);
					}
					else{
						HTML::help_build_individual_bleat($sorted_bleat_id, $username, $fullname, $profile_image, 
					    	$bleat, $time, $latitude, $longitude, $in_reply, $is_personal, $is_follow, 1, $file_one, $file_two, $file_three, $file_four);
					}
				    
				}
			}

			HTML::build_home_bleats_end();
		    HTML::build_pagination_bar("mentions", "$page_number", "$result_limit", "$total_page_number", "$pagination_limit");
		}
		else{
			HTML::build_no_bleat();
		}
	}
	else{
		HTML::build_no_bleat();
	}
}

# This function helps to get a response that is related to the user's bleat upwards.
# It is done by getting the in_reply_to field in every bleats that is related to the user's bleat.
sub recursive_up_response {
	my $bleat_original = $_[0] || "";
	my @bleat_responses = ();

	if (defined $global_all_user_bleats{"$bleat_original"}){
		my $in_reply 	= $global_all_user_bleats{"$bleat_original"}{'in_reply_to'};

		foreach my $bleat_id (keys % global_all_user_bleats){
			if (defined $in_reply &&  "$in_reply" eq "$bleat_id"){
				push(@bleat_responses, $bleat_id);
				recursive_up_response("$bleat_id");
			}
		}
	}

	return @bleat_responses;
}

# This function helps to get a response that is related to the user's bleat downwards.
# It is done by getting the in_reply_to field in every bleats that is related to the user's bleat.
sub recursive_down_response {
	my $bleat_original = $_[0] || "";
	my @bleat_responses = ();

	if (defined $global_all_user_bleats{"$bleat_original"}){
		foreach my $bleat_id (keys % global_all_user_bleats){
			my $in_reply 	= $global_all_user_bleats{"$bleat_id"}{'in_reply_to'};

			if (defined $in_reply && "$in_reply" eq "$bleat_original"){
				push(@bleat_responses, $bleat_id);
				recursive_down_response("$bleat_id");
			}
		}
	}

	return @bleat_responses;
}





# This function helps to get the user's information and displays it by calling an HTML function
sub get_account_information{
	my $username = $_[0];
	my $alert_message_change_info = $_[1];
	my $alert_message_change_pass = $_[2];

	my $alert_message_change_pp = $_[3];
	my $alert_message_change_bg = $_[4];

	my $alert_message_change_acc = $_[5];

	$username = remove_new_line_and_spaces($username);
	$username = lc($username);

    if (defined $global_all_users{"$username"}){
    	my $user_proper = $global_all_users{"$username"}{"username"};


    	my $email 		= $global_all_users{"$username"}{"email"};
    	my $full_name 	= $global_all_users{"$username"}{"full_name"};
    	my $suburb 		= $global_all_users{"$username"}{"home_suburb"};


    	my $latitude 	= $global_all_users{"$username"}{"home_latitude"};
    	my $longitude 	= $global_all_users{"$username"}{"home_longitude"};
    	my $prof_text 	= $global_all_users{"$username"}{"profile_text"};


    	my $notify_mention 	= $global_all_users{"$username"}{"notify_mention"};
    	my $notify_reply 	= $global_all_users{"$username"}{"notify_reply"};
    	my $notify_new_listen 	= $global_all_users{"$username"}{"notify_new_listen"};


		HTML::build_account_settings_page($user_proper, $email, $full_name, $suburb, $latitude, $longitude, 
			$prof_text, $notify_mention, $notify_reply, $notify_new_listen, 
			$alert_message_change_info, $alert_message_change_pass,
			$alert_message_change_pp, $alert_message_change_bg, $alert_message_change_acc);
    }
    else{
    	HTML::build_account_settings_page($username, "", "", "", "", "", "", "", "", "", 
    		$alert_message_change_info, $alert_message_change_pass, 
    		$alert_message_change_pp, $alert_message_change_bg, $alert_message_change_acc);
    }
}





# This function will help to update the user's account information (e.g. email, full name, etc.)
sub update_account_information {
	my $username = $_[0];
	my $lower_cased_username = lc("$username");

	my $email = $_[1];
    my $full_name = $_[2] || "";
    my $suburb = $_[3] || "";

	my $latitude = $_[4] || "";
    my $longitude = $_[5] || "";
    my $prof_text = $_[6] || "";

    $username = remove_new_line_and_spaces($username);
    $email = remove_new_line_and_spaces($email);
    $full_name = remove_new_line_and_spaces($full_name);
    $suburb = remove_new_line_and_spaces($suburb);

    $latitude = remove_new_line_and_spaces($latitude);
	$longitude = remove_new_line_and_spaces($longitude);
	$prof_text = remove_new_line_and_spaces($prof_text);

	$prof_text =~ s/\n|\r//g;

	foreach my $user (sort keys %global_all_users){
		my $email_svr 		= $global_all_users{"$user"}{"email"};

		if (lc("$email_svr") eq lc("$email") && lc("$user") ne "$lower_cased_username"){
			return 0;
		}
	}

	# Update Information here
	$global_all_users{"$lower_cased_username"}{"email"} = $email;
	$global_all_users{"$lower_cased_username"}{"full_name"} = $full_name;
	$global_all_users{"$lower_cased_username"}{"home_suburb"} = $suburb;

	$global_all_users{"$lower_cased_username"}{"home_latitude"} = $latitude;
	$global_all_users{"$lower_cased_username"}{"home_longitude"} = $longitude;
    $global_all_users{"$lower_cased_username"}{"profile_text"} = $prof_text; # Profile Text

    # Update Detail.txt File
    update_detail_file("$lower_cased_username", "email", "$email") or return 2;
    update_detail_file("$lower_cased_username", "full_name", "$full_name") or return 2;
    update_detail_file("$lower_cased_username", "home_suburb", "$suburb") or return 2;

    update_detail_file("$lower_cased_username", "home_latitude", "$latitude") or return 2;
    update_detail_file("$lower_cased_username", "home_longitude", "$longitude") or return 2;
    update_detail_file("$lower_cased_username", "profile_text", "$prof_text") or return 2;

	update_database(1) or return 2;
	build_database("no");

	return 1;
}





# This function helps to perform recover password when a user requested it and it will send an email to the user.
sub recover_password {
	my $username = $_[0];
	my $url_link = $_[1] || "";

	$username = remove_new_line_and_spaces($username);
	$url_link = remove_new_line_and_spaces($url_link);

	my $lower_cased_username = lc("$username");

	my $email = $global_all_users{"$lower_cased_username"}{"email"};
	$username = $global_all_users{"$lower_cased_username"}{"username"};		

	my $recover_code = Security::encode_data_md5("$lower_cased_username|" . time);
    $global_all_users{"$username"}{"recover_password_code"} = $recover_code; # Set Verifcation (Activation Code) for email purpose

	update_database(1) or return 0;
	build_database("no");

	my $message_email = <<email;
		Hey, $username\,
		\n
		You have requested to recover your password.
		\n
		Please recover your password by clicking the link below.
		\n
		$url_link?a=fp&rp_id=$lower_cased_username&rec=$recover_code
		\n
		\n
		If you didn't request to recover your password, just delete this email and everything will go back to the way it was.
email
	
	send_email("$email", "Bitter Recover Password", "$message_email") or return 2;
}





# This function helps to verify whether the change password action is valid using 'Recovery Code' to validate
sub change_password_verify {
    my $user = $_[0] || "";
    my $recover_code = $_[1] || "";

	$user = remove_new_line_and_spaces($user);
    $recover_code = remove_new_line_and_spaces($recover_code);

    $user = lc($user);

	if (defined $global_all_users{"$user"}){
		my $recovery_code  = $global_all_users{"$user"}{"recover_password_code"};

		if ("$recover_code" eq "$recovery_code"){
			return 1;
		}
		else{
			return 2;
		}
	}
	else{
		return 0; # Non-existant user
	}
}





# This function will help to change the user's password during 'Recovery Password'
sub change_password_recover {
    my $user = $_[0] || "";
    my $recover_code = $_[1] || "";
    my $new_password = $_[2] || "";

	$user = remove_new_line_and_spaces($user);
    $recover_code = remove_new_line_and_spaces($recover_code);
    $new_password = remove_new_line_and_spaces($new_password);

    $user = lc($user);

    my $result = change_password_verify($user, $recover_code);

    if ($result == 1){
		$global_all_users{"$user"}{"password"} = Security::encode_data_md5("$new_password");

	    update_detail_file("$user", "password", "$new_password") or return 2;

		update_database(1) or return 2;
		build_database("no");

		return 1;
    }
    else{
    	return $result;
    }
}





# This function will help to change the user's password
sub change_password {
    my $user = $_[0] || "";
    my $new_password = $_[1] || "";

	$global_all_users{"$user"}{"password"} = Security::encode_data_md5("$new_password");

    update_detail_file("$user", "password", "$new_password") or return 0;

	update_database(1) or return 0;
	build_database("no");

	return 1;
}





# This function will help to change a profile picture
sub change_profile_image {
    my $user = $_[0] || "";

	$user = remove_new_line_and_spaces($user);
    $user = lc($user);

	my $folder_name = $global_all_users{"$user"}{"username"};
	my $image_location = "$users_dir/$folder_name/profile.jpg";
	$global_all_users{"$user"}{"profile_pic"} = $image_location;

	update_database(1) or return 0;
	build_database("no");

	return 1;
}





# This function will help to delete a profile picture
sub delete_profile_image {
    my $user = $_[0] || "";

	$user = remove_new_line_and_spaces($user);
    $user = lc($user);

	$global_all_users{"$user"}{"profile_pic"} = $no_profile; # Set the profile pic property to no profile (global variable)
			
	my $folder_name = $global_all_users{"$user"}{"username"};
	my $image_location = "$users_dir/$folder_name/profile.jpg";

	if (-f $image_location){ # Check if profile image does exists
		unlink $image_location or return 0;
	}

	update_database(1) or return 0;
	build_database("no");

	return 1;
}





# This function will help to change a background image
sub change_background_image {
    my $user = $_[0] || "";

	$user = remove_new_line_and_spaces($user);
    $user = lc($user);

	my $folder_name = $global_all_users{"$user"}{"username"};
	my $image_location = "$users_dir/$folder_name/background.jpg";
	$global_all_users{"$user"}{"background_pic"} = $image_location;

	update_database(1) or return 0;
	build_database("no");

	return 1;
}





# This function will help to delete a background image
sub delete_background_image {
    my $user = $_[0] || "";

	$user = remove_new_line_and_spaces($user);
    $user = lc($user);

	$global_all_users{"$user"}{"background_pic"} = $default_background; # Set the profile pic property to no profile (global variable)
			
	my $folder_name = $global_all_users{"$user"}{"username"};
	my $image_location = "$users_dir/$folder_name/background.jpg";

	if (-f $image_location){ # Check if profile image does exists
		unlink $image_location or return 0;
	}

	update_database(1) or return 0;
	build_database("no");

	return 1;
}





# This function will help to delete a bleat
sub delete_bleat {
    my $user = $_[0] || "";
    my $bleat_id = $_[1] || "";

	$user = remove_new_line_and_spaces($user);
    $user = lc($user);

	$bleat_id = remove_new_line_and_spaces($bleat_id);
    $bleat_id = lc($bleat_id);

    if ($user && $bleat_id){
    	delete $global_all_user_bleats{"$bleat_id"} or return 0;

		my @old_bleats = @{$global_all_users{"$user"}{"all_bleats"}};
		my @new_bleats = ();

		foreach my $ind_bleat (@old_bleats){
			if (lc("$bleat_id") ne lc("$ind_bleat")){
				push(@new_bleats, $ind_bleat);
			}
		}

		$global_all_users{"$user"}{"all_bleats"} = [ @new_bleats ];

    	my $folder_name = $global_all_users{"$user"}{"username"};

		open(my $bleat_txt,  ">", "$users_dir/$folder_name/bleats.txt") or return 0; # Update User Bleats in Bleats.txt
		print $bleat_txt join("\n", @new_bleats);
		close $bleat_txt;

		my $bleat_location = "$bleats_dir/$bleat_id";
		if (-f $bleat_location){ # Check if profile image does exists
			unlink $bleat_location or return 0;
		}

		foreach my $file (glob("$upload_dir/$bleat_id" . "_*")) {
			unlink $file or return 0;
		}

		update_database(1) or return 0;
		update_database(2) or return 0;
		update_database(3) or return 0;

		build_database("no");

		return 1;
    }

	return 0;
}





# This function helps to suspend / unsuspend a user account
sub suspend_unsuspend_account {
    my $user = $_[0] || "";
    my $should_suspend = $_[1] || 0;
 
	$user = remove_new_line_and_spaces($user);
    $user = lc($user);

    if ($should_suspend){
	    $global_all_users{"$user"}{"status"} = "suspend"; # Status : Verify (Sign Up Verification Required), Valid (Normal), Suspend (Invincible Account)
	}
	else{
	    $global_all_users{"$user"}{"status"} = "valid"; # Status : Verify (Sign Up Verification Required), Valid (Normal), Suspend (Invincible Account)
	}

	update_database(1) or return 0;
    build_database("no");

	return 1;
}





# This function helps to delete a user account
sub delete_account {
	my $user = $_[0] || "";

	$user = remove_new_line_and_spaces($user);
    $user = lc($user);

    my @all_bleats 	= @{ $global_all_users{"$user"}{"all_bleats"} }; # Hashes of Hashes
	foreach my $bleat_id (@all_bleats){
		$bleat_id = remove_new_line_and_spaces($bleat_id); # There is new line in the Bleat ID
		$bleat_id = lc($bleat_id);

		delete $global_all_user_bleats{"$bleat_id"} or return 0;

		my $each_bleat_file = "$bleats_dir/$bleat_id";
		if (-f $each_bleat_file){ # Check if bleat file exists
			unlink $each_bleat_file or return 0; # This will delete all of the user's bleats file
		}
	}

	my $folder_name = $global_all_users{"$user"}{"username"};
	my $details_txt = "$users_dir/$folder_name/details.txt";
	my $bleats_file = "$users_dir/$folder_name/bleats.txt";
	my $profile_image = "$users_dir/$folder_name/profile.jpg";
	my $background_image = "$users_dir/$folder_name/background.jpg";

	if (-f $details_txt){ # Check if details.txt does file exists
		unlink $details_txt or return 0; #If it exists, it will delete the details.txt file
	}

	if (-f $bleats_file){# Check if bleats.txt does file exists
		unlink $bleats_file or return 0; #If it exists, it will delete bleats.txt file
	}

	if (-f $profile_image){ # Check if profile picture does exists
		unlink $profile_image or return 0; #If it exists, it will delete the profile picture
	}

	if (-f $background_image){ # Check if background image does exists
		unlink $background_image or return 0; # If it exists, it will delete the background image
	}

	if (-d "$users_dir/$folder_name"){ # Check if the user's folder exists
		rmdir "$users_dir/$folder_name" or return 0; # If it exists, it will delete the user's folder
	}

	delete $global_all_users{"$user"} or return 0; # Remove the user from the hash

	# Update the database
	update_database(1) or return 0;
	update_database(2) or return 0;
	update_database(3) or return 0;

	# Rebuild the databse
	build_database("no");

	return 1;
}





# This function helps to update the notification settings for a particular user.
sub nofication_update {
	my $user 				= $_[0] || "";
    my $notify_mention 		= $_[1] || "off";
    my $notify_reply 		= $_[2] || "off";
    my $notify_new_listen 	= $_[3] || "off";

	$notify_mention = remove_new_line_and_spaces($notify_mention);
    $notify_mention = lc($notify_mention);

	$notify_reply = remove_new_line_and_spaces($notify_reply);
    $notify_reply = lc($notify_reply);

	$notify_new_listen = remove_new_line_and_spaces($notify_new_listen);
    $notify_new_listen = lc($notify_new_listen);

    $global_all_users{"$user"}{"notify_mention"} 	= $notify_mention;
    $global_all_users{"$user"}{"notify_reply"} 		= $notify_reply;
    $global_all_users{"$user"}{"notify_new_listen"} = $notify_new_listen;

    update_database(1) or return 0;
	build_database("no");

	return 1;
}





# This function will be called when the user's is being mentioned in a bleat by another user.
# This function will send out email to the user if they allow themselves to receive notification.
sub notification_mentioned {
	my $mentioned = $_[0];
	my $mentioner = $_[1];

	my $bleat_message = $_[2];

	$mentioned = remove_new_line_and_spaces($mentioned);
    $mentioned = lc($mentioned);

    $mentioner = remove_new_line_and_spaces($mentioner);
    $mentioner = lc($mentioner);

	$bleat_message = remove_new_line_and_spaces($bleat_message);

    $mentioned = $global_all_users{"$mentioned"}{"username"};
    $mentioner = $global_all_users{"$mentioner"}{"username"};

    if ($mentioned){
	    my $notify_mention = $global_all_users{"$mentioned"}{"notify_mention"};
	    my $email = $global_all_users{"$mentioned"}{"email"};

		my $message_email = <<email;
				Hey, $mentioned\,
				\n
				You have been mentioned in a bleat post made by $mentioner.
				\n
				$bleat_message
				\n
				\n
				If you didn't have a Bitter account, just delete this email and everything will go back to the way it was.
email
			
			if ($notify_mention eq "on"){
				send_email("$email", "Someone mentioned you on Bitter", "$message_email") or return 0;
			}	

			return 1;
	}

	return 0;
}





# This function will be called when the user's bleat is being replied by another user.
# This function will send out email to the user if they allow themselves to receive notification.
sub notification_reply {
	my $mentioned = $_[0];
	my $mentioner = $_[1];

	my $bleat_message = $_[2];

	$mentioned = remove_new_line_and_spaces($mentioned);
    $mentioned = lc($mentioned);

    $mentioner = remove_new_line_and_spaces($mentioner);
    $mentioner = lc($mentioner);

	$bleat_message = remove_new_line_and_spaces($bleat_message);

    $mentioned = $global_all_users{"$mentioned"}{"username"};
    $mentioner = $global_all_users{"$mentioner"}{"username"};

    if ($mentioned){
	    my $notify_mention = $global_all_users{"$mentioned"}{"notify_reply"};
	    my $email = $global_all_users{"$mentioned"}{"email"};

		my $message_email = <<email;
				Hey, $mentioned\,
				\n
				Someone has replied to your post on Bitter.
				\n
				$bleat_message
				\n
				\n
				If you didn't have a Bitter account, just delete this email and everything will go back to the way it was.
email
			
			if ($notify_mention eq "on"){
				send_email("$email", "Someone reply your post on Bitter", "$message_email") or return 0;
			}	

			return 1;
	}

	return 0;
}





# This function will be called when the user is being followed (listened) by another user.
# This function will send out email to the followed user if they allow themselves to receive notification.
sub notification_being_listened {
	my $mentioned = $_[0];
	my $mentioner = $_[1];

	$mentioned = remove_new_line_and_spaces($mentioned);
    $mentioned = lc($mentioned);

    $mentioner = remove_new_line_and_spaces($mentioner);
    $mentioner = lc($mentioner);

    $mentioned = $global_all_users{"$mentioned"}{"username"};
    $mentioner = $global_all_users{"$mentioner"}{"username"};

    if ($mentioned){
	    my $notify_mention = $global_all_users{"$mentioned"}{"notify_reply"};
	    my $email = $global_all_users{"$mentioned"}{"email"};

		my $message_email = <<email;
				Hey, $mentioned\,
				\n
				$mentioner has started following you on Bitter.
				\n
				\n
				If you didn't have a Bitter account, just delete this email and everything will go back to the way it was.
email
			
			if ($notify_mention eq "on"){
				send_email("$email", "Someone started following you on Bitter", "$message_email") or return 0;
			}	

			return 1;
	}

	return 0;

}





# This function helps to get the location of the uploaded file used for bleats
sub get_upload_file_location {
	my $bleat_id = $_[0];

	my $file_one = "";
	my $file_two = "";
	my $file_three = "";
	my $file_four = "";

	if (glob("$upload_dir/$bleat_id" . "_" . "1" . "*")) {
		$file_one = glob("$upload_dir/$bleat_id" . "_" . "1" . "*");
	}

	if (glob("$upload_dir/$bleat_id" . "_" . "2" . "*")) {
		$file_two = glob("$upload_dir/$bleat_id" . "_" . "2" . "*");
	}

	if (glob("$upload_dir/$bleat_id" . "_" . "3" . "*")) {
		$file_three = glob("$upload_dir/$bleat_id" . "_" . "3" . "*");
	}

	if (glob("$upload_dir/$bleat_id" . "_" . "4" . "*")) {
		$file_four = glob("$upload_dir/$bleat_id" . "_" . "4" . "*");
	}

	return ($file_one, $file_two, $file_three, $file_four);
}





# This function will help to update the Details.txt given a key and the new value.
sub update_detail_file {
	my $username_original = $_[0];
	my $key_to_update = $_[1];
	my $new_value = $_[2]; # Note : Array must be flattened

	my $folder_name = $global_all_users{"$username_original"}{"username"};
	my $filename = "$users_dir/$folder_name/details.txt";

	my $detail_content = "";
	my $found_key = 0;

	if (open(my $fh, $filename)) {
		while (my $row = <$fh>) {
			$row = remove_new_line_and_spaces($row);

			if ($row =~ /^$key_to_update\:/){
				$detail_content .= "$key_to_update\: $new_value\n";
				$found_key = 1;
			}	
			else{
				$detail_content .= "$row\n";
			}
		}

		if (not $found_key){
			$detail_content .= "$key_to_update\: $new_value\n"; # Key that does not exists
		}

		open(my $detail_txt,  ">", "$users_dir/$folder_name/details.txt") or return 0; # Update User Bleats in Bleats.txt
		print $detail_txt $detail_content;
		close $detail_txt;

		return 1;
	}
	else {
		return 0;
	}
}





# This function will determine whether it is appropriate to display the follow button.
# This function will allow follow to be displayed when a given user is not the logged in user.
sub is_follow_display{
	my $username = $_[0];

	$username = remove_new_line_and_spaces($username);
	$username = lc($username);

	if ($ENV{HTTP_COOKIE} =~ /\buser=([A-Za-z0-9\_\-\.]+)/ ) {
		my $current_user = $1;

		return 0 if $current_user ne $username;
	}

	return 1;
}





# This function will helps to get the currently logged in user.
sub get_currently_logged_in_user{
	if ($ENV{HTTP_COOKIE} =~ /\buser=([A-Za-z0-9\_\-\.]+)/ ) {
		my $current_user = $1;

		return $current_user;
	}

	return "";
}





# This function will help to determine whether the current logged in user is following another user.
# The username is passed in is the username of other.
sub is_currrently_following{
	my $another_user = $_[0];

	$another_user = remove_new_line_and_spaces($another_user);
	$another_user = lc($another_user);

	if ($ENV{HTTP_COOKIE} =~ /\buser=([A-Za-z0-9\_\-\.]+)/ ) {
		my $current_user = $1;

		my @listen_to_profile = @{ $global_all_users{lc("$current_user")}{"listens"} }; # Hashes of Hashes

		if (grep(/^$another_user$/i, @listen_to_profile)) {	
			return 1;
		}		
	}

	return 0;
}





# This function will help to send email to a given recepient, subject and message.
sub send_email{
	my $recepient = $_[0];
	my $subject = $_[1];
	my $message = $_[2];

	open(MAIL, "|/usr/sbin/sendmail -t") or return 1;
	 
	# Email Header
	print MAIL "To: $recepient\n";
	print MAIL "Subject: $subject\n\n";

	# Email Body
	print MAIL "$message";

	close(MAIL);

	return 1;
}





# This function will help to round a number up.
# This function will help to determine the number of pages for pagination.
sub roundup {
    my $n = shift;
    return(($n == int($n)) ? $n : int($n + 1))
}





# This function will help to remove new line(s) and trailing spaces in a given data variable.
sub remove_new_line_and_spaces {
	my $data = $_[0];
	chomp($data);

	$data =~ s/^\s+//;
	$data =~ s/\s+$//;

	return $data;
}



# Debug Code to Check Databse size
# Get the total number of users in Bitter
sub get_total_users{
	# Debug data
	my $size = keys %global_all_users;
	return $size;
}

# Get the total number of bleats posted in Bitter
sub get_total_bleats{
	# Debug data
	my $size = keys %global_all_user_bleats;
	return $size;
}

# Get the latest id bleat used
sub get_latest_bleat_id{
	# Debug data
	return $latest_bleat_id;
}

# Get the total number of details of a user
sub get_total_user_details{
	my $username = $_[0];
	$username = remove_new_line_and_spaces($username);
	$username = lc($username);
	# Note : Size becomes +2 because of Bleats & Profile Pic keys

	# Debug data
	my $size = scalar keys %{$global_all_users{"$username"}};
	$size = $size - 2;
	return $size;
}

# Get the total number of bleats made by the user
sub get_total_user_bleats{
	my $username = $_[0];
	$username = remove_new_line_and_spaces($username);
	$username = lc($username);

	# Debug data
	my @size = @{$global_all_users{"$username"}{"all_bleats"}};
	return @size;
}

1;