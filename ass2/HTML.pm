#!/usr/bin/perl -w

#
# Written by Timothy Putra Pringgondhani (z5043683)
# z5043683@zmail.unsw.edu.au
#
# Created on 12 October 2015
# Last Update on 25 October 2015 9:56 PM
#

# This Perl Module is designed to provide to handle all of the HTML source code.
# This module is used when Bitter want to print out the HTML code.
#

use strict;

use Bitter; # User Bitter package so that it can call Bitter.pm function(s)

package HTML;


# Redirect command for HTML (does not apply on CGI Server)
sub redirect_page{
	my $page = $_[0] || "./bitter.cgi";

	print <<redirect;
	Content-Type: text/html; charset=utf-8
	Cache-Control: no-cache, no-store, must-revalidate
	Pragma: no-cache
	Expires: 0

	<!DOCTYPE html>
	<html lang="en">
		<head>
			<script type="text/javascript">
				window.location.href = "$page"
			</script>
		</head>
	</html>
redirect
}





# Print the Header of HTML.
# The title of the page is given as a paramater so that each page has different title.
sub header{
	my $title = $_[0];

	print <<Header;
Content-Type: text/html; charset=utf-8
Cache-Control: no-cache, no-store, must-revalidate
Pragma: no-cache
Expires: 0

<!DOCTYPE html>
<html lang="en">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
	    <meta name="viewport" content="width=device-width, initial-scale=1">
	    
		<title>$title</title>

		<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css" rel="stylesheet">
		<link href="./css/bitter.css" rel="stylesheet">

		<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
		<script type="text/javascript" src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>

	</head>
Header
}





# Print the top navigation part of each page
sub navigation {
	my $location = $_[0];
	my ($home_active, $login_active, $search_active, $mention_active, $about_active, $write_active, $signup_active) = ("noactive") x7;

	$home_active 	= "active" if ($location eq "Home");
	$login_active 	= "active" if ($location eq "Login");
	$search_active 	= "active" if ($location eq "Search");
	$mention_active	= "active" if ($location eq "Mentions");
	$about_active 	= "active" if ($location eq "About");
	$write_active 	= "active" if ($location eq "Write");
	$signup_active  = "active" if ($location eq "SignUp");

	# Checks whether the user is logged in by checking that there is 'auth_token' and 'user' in the cookie.
	# If the user is logged in, it will print out the navigation bar of a logged in user.
	if ($ENV{HTTP_COOKIE} =~ /\bauth_token=\S+/ && $ENV{HTTP_COOKIE} =~ /\buser=([A-Za-z0-9\_\-\.]+)/) {
		my $fullname 			= "";
		my $username 			= "";
		my $profile_image 		= "";
		my $user = $1;

		if (defined $user && $user){
			$user = lc($user);

			Bitter::build_database(); # This will build the database

			$username = $Bitter::global_all_users{"$user"}{"username"} || "";
			$fullname = $Bitter::global_all_users{"$user"}{"full_name"} || "";
			$profile_image = $Bitter::global_all_users{"$user"}{"profile_pic"} || $Bitter::no_profile;
			$profile_image = "./" . $profile_image;
			
			my $background_image = $Bitter::global_all_users{"$user"}{"background_pic"} || $Bitter::default_background;
			$background_image = "./" . $background_image;

			print <<Navigation;
			<body background="$background_image">
				<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
				    <div class="container">
				        <div class="navbar-header">
				            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar">
				                <span class="sr-only">Toggle navigation</span>
				                <span class="icon-bar"></span>
				                <span class="icon-bar"></span>
				                <span class="icon-bar"></span>
				            </button>
				            <a class="navbar-brand" href="bitter.cgi">
				            	<span class="glyphicon glyphicon-fire"></span> 
				            	Bitter
				            </a>
				        </div>
				        <div class="collapse navbar-collapse" id="navbar">
				            <ul class="nav navbar-nav">
				                <li class="$home_active">
				                    <a href="bitter.cgi">Home</a>
				                </li>
				                <li class="$mention_active">
				                    <a href="mentions.cgi">Mentions</a>
				                </li>
				                <li class="$search_active">
				                    <a href="search.cgi">Search</a>
				                </li>
        		                <li class="$about_active">
				                    <a href="about.cgi">About</a>
				                </li>
				            </ul>
				            <ul class="nav navbar-nav navbar-right">
				            	<li class="dropdown navbar-right">
									<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
										<img class="img-rounded" height="20" width="20" src="$profile_image" alt=""> 
	 									<span class="caret"></span>
									</a>
									<ul class="dropdown-menu" aria-labelledby="about-us">
										<li>
											<a style="color:#0099FF" href="profile.cgi">
												$fullname
												<h6 style="color:#0099FF">\@$username</h6>
											</a>
										</li>
										<li>
											<a href="settings.cgi">Account Settings</a>
										</li>
										<li>
											<a href="logout.cgi">Log Out</a>
										</li>
									</ul>
								</li>
								<li class="navbar-right $write_active">
				            		 <a href="write.cgi">Write Bleat</a>
				            	</li>
				            </ul>
				        </div>
				    </div>
				</nav>
Navigation

			return;
		}
	}

	# This will be printed when the user is not logged in.
	print <<Navigation;
	<body background="./assets/default_back.jpg">
		<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
		    <div class="container">
		        <div class="navbar-header">
		            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar">
		                <span class="sr-only">Toggle navigation</span>
		                <span class="icon-bar"></span>
		                <span class="icon-bar"></span>
		                <span class="icon-bar"></span>
		            </button>
		            <a class="navbar-brand" href="bitter.cgi">
		            	<span class="glyphicon glyphicon-fire"></span> 
		            	Bitter
		            </a>
		        </div>
		        <div class="collapse navbar-collapse" id="navbar">
		            <ul class="nav navbar-nav">
		                <li class="$home_active">
		                    <a href="bitter.cgi">Home</a>
		                </li>
		                <li class="$login_active">
		                    <a href="login.cgi">Login</a>
		                </li>
						<li class="$signup_active">
		                    <a href="signup.cgi">Sign Up</a>
		                </li>
		                <li class="$about_active">
		                    <a href="about.cgi">About</a>
		                </li>
		            </ul>
		        </div>
		    </div>
		</nav>
Navigation

}





# Print the Footer of HTML. It shows the copyright of the page.
sub footer {
	my $all_script = join("\n", @_);

	print <<Footer;
		<footer>
			<div class="small-print" style="background-color:transparent">
				<div class="container">
		    		<p style="color:White">
		    			Copyright &copy; 2015 Timothy Putra Pringgondhani
		    		</p>
		    	</div>
		    </div>
		</footer>
		$all_script
	</body>
</html>
Footer
}





# Display the Profile not Found Page
# It will show a page that the user is not found and shows a search bar.
# When a user is not found, it is likely that the user does not exists.
sub profile_not_found{
	my $search_value = $_[0]; # This variable is used to indicate the information that the user is looking for

    print <<NotFound;
	<div class="container">
	    <div class="row">
	        <div class="col-lg-12">
		
				<h1 class="page-header">
					<center style="color:White">Sorry, that profile doesn’t exist!</center>
				</h1>
				<h4>
					<center style="color:White">
						You can <a style="color:#0099FF" href="search.cgi">search Bitter</a> 
						using the search box below or <a style="color:#0099FF" href="home.cgi">return to the homepage</a>.
					</center>
				</h4>
				<center>
					<div class="col-xs-12 col-lg-12">
						<form class="navbar-form form-group" role="search" action="search.cgi" method="GET">
							<div class="form-group">
								<input type="text" class="form-control" name="q" value="$search_value">
								<input type="hidden" class="form-control" name="qo" value="profile">
								<button type="submit" class="btn btn-default">Search</button>
							</div>
						</form>
					</div>
				</center>
	        </div>
	    </div>
	</div>
NotFound
}





# This function displays the about page.
sub build_about_page {
	print <<About;
	<div class="container">
	    <div class="row">
	        <div class="col-lg-12">
		
				<h1 class="page-header">
					<center style="color:White">Bitter Story</center>
				</h1>
				<center>
					<div class="col-xs-12 col-lg-12 img-rounded" style="background-color:White">
						<p>
							<h3 style="color:#0099FF">
								What is Bitter? Let me tell you a story...
							</h3>
						</p>
						<p>
							<h4 style="color:#0099FF">
								<u>History</u>
							</h4>
						</p>
						<p>
							<b>Andrew</b> has noticed students are often complaining about COMP[29]041.
						</p>
						<p>
							Rather than address their complaints and improve COMP2041, <b>Andrew</b> has decided he will make himself rich exploiting COMP[29]041 coding skills and then give up lecturing.
						</p>
						<p>
							<b>Andrew</b>'s plan is to have COMP[29]041 students create a social media platform called Bitter for complaints.
						</p>
						<p>
							<b>Andrew</b> believes the absence of any similar existing social media platform means Bitter will become very popular and he will become rich.
						</p>
						<p>
							<h4 style="color:#0099FF">
								<u>Story</u>
							</h4>
						</p>
						<p>
							<b>Bitter - A social media website that was built with bitterness.</b>
						</p>
						<p>
							Made by Timothy Pringgondhani
						</p>
					</div>
				</center>
	        </div>
	    </div>
	</div>
About
}





# This function helps to build the profile page of a user.
sub build_profile {
	my $username 		= $_[0];
	my $full_name 		= $_[1] || "";
	my $profile_image 	= "./" . $_[2];
	my $listen_to 		= $_[3];
	my $latitude		= $_[4];
	my $longitude		= $_[5];
	my $suburb			= $_[6];
	my $bleats			= $_[7];

	my $is_personal		= $_[8];
	my $is_follow		= $_[9];

	my $prof_text		= $_[10];

	my $follow_unfollow	= "";
	my $profile_other = build_profile_other($latitude, $longitude, $suburb, $prof_text); # This will call a function that will display information of the user.

	if (!$is_personal){ # This checks whether the profile is the user's or other's. If it is other's it will display the follow/unfollow button.
		my $display_text = "Follow";
		my $should_follow = "yes";

		if ($is_follow){
			$display_text = "Following";
			$should_follow = "no";
		}

		$follow_unfollow = qq(
				<h3>
					<form action="profile.cgi" method="POST">
						<input type="hidden" name="user" id="user" value="$username"></input>
						<input type="hidden" name="follow" id="follow" value="$should_follow"></input>
						<button type="submit" class="btn btn-default" style="width:150px;" id="follow_button">$display_text</button></a>
					</form>
				</h3>
			);
	}

	# This will print out the HTML code of the information of the user.
	print <<Profile;
	<div class="container">
	    <div class="row page-header">
	        <div class="col-xs-12 col-lg-8">
	            <h1 style="color:White">
					$full_name 
	            </h1>
	        </div>
	    </div>
	    <div class="row">
	        <div class="col-xs-12 col-lg-4">
	        	<center>
				    <img class="img-responsive img-rounded" src="$profile_image" alt="">

					$follow_unfollow

		            <h3 class="modal-header">
		                <a style="color:#0099FF" href="profile.cgi">\@$username</a>
		            </h3>
		            <h4 class="modal-header">
		            	<a id="bleat-toggle" class="profile-a" onclick="return (false);">Bleats : <b>$bleats</b></a>
		            </h4>
		            <h4 class="modal-header">
		            	<a id="following-toggle" class="profile-a" onclick="return (false);">Following : <b>$listen_to</b></a>
		            </h4>
		            $profile_other
	            </center>
	        </div>
Profile
}





# This function will build the other information which is necessary for the user profile such as the location, suburb and description of the user.
sub build_profile_other {
	my $latitude		= $_[0];
	my $longitude		= $_[1];
	my $suburb			= $_[2];
	my $prof_text		= $_[3];

	my $google_maps 	= "";

	if (length($latitude) > 0 && length($longitude) > 0){
		$google_maps = "<p class=\"modal-header\">" . help_build_location_info("See Location on Google Maps", $latitude, $longitude) . "</p>";
	}

	if (length($suburb) > 0){
		$suburb = qq(<p><h4 style=\"color:White\"><b>Suburb</b> : $suburb</h4></p>);
	}

	if (length($prof_text) > 0){
		$prof_text = qq(<p>
				<div class="img-rounded" style="background-color:White">
					<br/>
					<p><h4 style="color:#0099FF"><b><u>About Me</u></b></h4></p>
					<p>$prof_text</p>
					<br/>
				</div>
			</p>);
	}

	return <<Profile;
				<div>
					$suburb
					$google_maps
					$prof_text
				</div>
Profile
}





# This function helps to print out the start of the bleat area in profile page
sub build_bleats_start {
	print <<Bleats_Start;
			<div id="bleat-toggle-div" class="col-xs-12 col-lg-8 img-rounded" style="background-color:White">
Bleats_Start
}





# This function helps to print out the end of the bleat area in profile page
sub build_bleats_end {
	print <<Bleats_End
			</div>
Bleats_End
}





# This function helps to print out the start of the user's following list area
sub build_listening_start {
	print <<Listening_Start;
			<div id="following-toggle-div" class="col-xs-12 col-lg-8 img-rounded" style="display:none; background-color:White;">
Listening_Start
}





# This function helps to print out the end of the user's following list area
sub build_listening_end {
	print <<Listening_End;
		    </div>
		</div>
Listening_End
}





# This function helps to print out the start of the bleat area in home page
sub build_home_bleats_start {
	print <<Bleats_Start;
		<div class="container">
	    <div class="row page-header"></div>
	    <div class="row">
				<div class="col-xs-10 col-lg-10 col-lg-offset-1 col-xs-offset-1 img-rounded" style="background-color:White">
Bleats_Start
}





# This function helps to print out the end of the bleat area in home page
sub build_home_bleats_end {
	print <<Bleats_End
				</div>
		    </div>
	</div>
Bleats_End
}





# This function helps to print out the start of the conversation bleat.
sub build_conversation_bleat_start {
	my $make_bleat_noticable = $_[0]; # THis variable will help to determine whether to make a bleat post prominent or not.

	if ($make_bleat_noticable){
		print <<Bleats_Start;
		<br/>
		<div class="container">
	    <div class="row">
				<div class="col-xs-10 col-lg-10 col-lg-offset-1 col-xs-offset-1 img-rounded" style="background-color:White">
Bleats_Start
	}
	else{
		print <<Bleats_Start;
		<br/>
		<div class="container">
	    <div class="row">
				<div class="col-xs-10 col-lg-10 col-lg-offset-1 col-xs-offset-1 img-rounded" style="background-color:#F0F0F0">
Bleats_Start
	}
}





# This function helps to print out individual bleat in a nicely formatted way.
sub help_build_individual_bleat {
	my $bleat_id 		= $_[0];
	my $username 		= $_[1];
	my $fullname 		= $_[2];
	my $profile_image	= "./" . $_[3];
	my $bleat 			= $_[4];
	my $time 			= convert_epoch_to_bleat_date($_[5]);
	my $latitude		= $_[6];
	my $longitude		= $_[7];
	my $in_reply		= $_[8];
	my $is_personal		= $_[9];
	my $is_follow		= $_[10];

	my $display_follow_btn = $_[11];

	my $file_one = $_[12];
	my $file_two = $_[13];
	my $file_three = $_[14];
	my $file_four = $_[15];

	my $google_maps   = help_build_location_info("Location", $latitude, $longitude); # This will call out a function that will display the location info.
	my $button_follow = "";
	my $delete_button = "";

	if (!$is_personal && $display_follow_btn){ # This checks whether there is a need to display a follow / unfollow button on each bleat
		my $display_text = "Follow";
		my $should_follow = "yes";
		my $color = "\#0099FF";

		if ($is_follow){
			$display_text = "Unfollow";
			$should_follow = "no";
			$color = "Red";
		}

		$button_follow = qq(
				<form class="form-horizontal" action="home.cgi" method="POST">
					|
						<input type="hidden" name="user" id="user" value="$username"></input>
						<input type="hidden" name="follow" id="follow" value="$should_follow"></input>
						<button type="submit" class="nostyle" style="color:$color">$display_text</button>
				</form>
		);
	}
	elsif ($is_personal){
		$delete_button = qq(| <a style=\"color:Red" href="action.cgi?a=db&b_id=$bleat_id">Delete</a>);
	}
	

	# Hashtags below will redirect to search page (search.cgi) upon clicking.
	$bleat =~ s/([^\:\&]?)\#(\w+)/$1<a style=\"color:\#0099FF\" href=\".\/search\.cgi\?q\=\%23$2&qo\=bleat\">\#$2<\/a>/g; # Handle hashtags and special characters. This will convert a hashtag into a clickable link into the search page.
	$bleat =~ s/@([A-Za-z0-9\_\-\.]+)/<a style=\"color:\#0099FF\" href=\"\.\/profile\.cgi\?user\=$1\">\@$1<\/a>/g; # This will convert a username in a bleat into a clickable link to the user's profile page.
		

	if (length($google_maps) > 0){
		$google_maps = "| $google_maps";
	}


	if (not defined $fullname){
		$fullname = "";
	}


	$in_reply = "| <a style=\"color:\#0099FF\" href=\"conversation.cgi?id=$bleat_id\">See Conversation</a>";
	my $reply = "| <a style=\"color:\#0099FF\" href=\"write.cgi?rt=$bleat_id\">Reply this Bleat</a>";

	if (defined $file_one){
		if ($file_one =~ /mp4$/){
			$file_one = qq(<p><video height="50%" width="50%" controls><source src="./$file_one" type="video/mp4"></source></video></p>);
		}
		elsif ($file_one =~ /(jpeg|jpg)$/){
			$file_one = qq(<p><img height="50%" width="50%" class="img-rounded" src="./$file_one" alt=""></p>);
		}
	}

	if (defined $file_two){
		if ($file_two =~ /mp4$/){
			$file_two = qq(<p><video height="50%" width="50%" controls><source src="./$file_two" type="video/mp4"></source></video></p>);
		}
		elsif ($file_two =~ /(jpeg|jpg)$/){
			$file_two = qq(<p><img height="50%" width="50%" class="img-rounded" src="./$file_two" alt=""></p>);
		}
	}

	if (defined $file_three){
		if ($file_three =~ /mp4$/){
			$file_three = qq(<p><video height="50%" width="50%" controls><source src="./$file_three" type="video/mp4"></source></video></p>);
		}
		elsif ($file_three =~ /(jpeg|jpg)$/){
			$file_three = qq(<p><img height="50%" width="50%" class="img-rounded" src="./$file_three" alt=""></p>);
		}
	}

	if (defined $file_four){
		if ($file_four =~ /mp4$/){
			$file_four = qq(<p><video height="50%" width="50%" controls><source src="./$file_four" type="video/mp4"></source></video></p>);
		}
		elsif ($file_four =~ /(jpeg|jpg)$/){
			$file_four = qq(<p><img height="50%" width="50%" class="img-rounded" src="./$file_four" alt=""></p>);
		}
	}


	print <<individual;
	<p>
		<h4 style="color:Silver">
			<img class="img-rounded" height="32" width="32" src="$profile_image" alt=""> 
				<a style="color:#0099FF" href="profile.cgi?user=$username">$fullname</a> \@$username
		</h4>
	</p>
	<p>
		<h4 style="color:SlateGray">$bleat</h4>
		$file_one
		$file_two
		$file_three
		$file_four
	</p>
	<p>$time $google_maps $in_reply</br>$reply $delete_button $button_follow</p>
	<p style="padding:0px" class="modal-header"></p>
individual
}





# Convert an Epoch Time to Date Format in aa nicely formatted way.
sub convert_epoch_to_bleat_date{
	my $time = $_[0];
	chomp($time);

	my @months = ("Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec");
	my ($sec, $min, $hour, $day, $month, $year) = (localtime($time))[0,1,2,3,4,5]; 

	if (($hour - 12) >= 0){
		$hour = $hour - 12 if ($hour > 12);
		return sprintf ("Bleatted on %02d:%02d PM - $day " . $months[$month] . " " . ($year + 1900), $hour, $min);
	}
	else{
		return sprintf ("Bleatted on %02d:%02d AM - $day " . $months[$month] . " " . ($year + 1900), $hour, $min);
	}
}





# This function will help to build the location info of the user's page.
sub help_build_location_info{
	my $caption			= $_[0];
	my $latitude		= $_[1];
	my $longitude		= $_[2];
	my $google_maps 	= "https://www.google.com/maps?q=";

	if (defined $latitude || defined $longitude){
		$google_maps .= "$latitude," if (length($latitude) > 0 && defined $latitude);
		$google_maps .= "$longitude" if (length($longitude) > 0 && defined $longitude);

		return <<location
		<a style="color:#0099FF" href="$google_maps">
			<img class="img-rounded" height="22" width="22" src="./assets/location.png" alt=""> 
			$caption
		</a>
location
	}

	return "";
}





# This function is called when there is no bleat in a current page. It will show 'no bleat to be displayed currently'.
sub build_no_bleat {
	print <<nobleat;
		<div class="container">
			<div class="row">
				<h1 class="page-header"></h1>
			</div>
		    <div class="row">
			    <div class="col-lg-12">
					<div class="col-xs-12 col-lg-10 col-lg-offset-1 img-rounded" style="background-color:White">
						<p>	
							<center>
								<h4 style="color:Red">
									There is no bleat to be displayed right now. <a href="write.cgi">Start writing Bleat</a> or <a href="search.cgi">Search for your friend.</a>
								</h4>
							</center>
						</p>
					</div>
		        </div>
		    </div>
		</div>
nobleat
}





# This function is called when there is no bleat made by a user in a profile page. It will show that no bleat to be displayed in the profile message.
sub build_no_bleat_profile{
	print <<nobleat;
			<div id="bleat-toggle-div" class="col-xs-12 col-lg-8 img-rounded" style="background-color:White">
				<p>	
					<center>
						<h4 style="color:Red">
							You have no bleats right now. <a href="write.cgi">Start writing Bleat</a> or <a href="search.cgi">Search for your friend.</a>
						</h4>
					</center>
				</p>
			</div>
nobleat
}





# THis function is called when a user does not follow another user. It will show that a user does not follow anyone message.
sub build_no_listening {
	print <<nolistening;
			<div id="following-toggle-div" class="col-xs-12 col-lg-8 img-rounded" style="display:none; background-color:White">
				<p>	
					<center>
						<h4 style="color:Red">
							Not listening to anyone...
						</h4>
					</center>
				</p>
			</div>
	    </div>
	</div>
nolistening
}





# This function helps to display individual profile that the user is currently following
sub build_individual_listening {
	my $username 		= $_[0];
	my $fullname 		= $_[1];
	my $profile_image	= "./" . $_[2];
	my $listen_to		= $_[3];
	my $is_personal		= $_[4];
	my $is_follow		= $_[5];

	my $button_follow = "";

	if (!$is_personal){
		my $display_text = "Follow";
		my $should_follow = "yes";
		my $color = "\#0099FF";

		if ($is_follow){
			$display_text = "Unfollow";
			$should_follow = "no";
			$color = "Red";
		}

		$button_follow = qq(
			<form class="form-horizontal" action="profile.cgi" method="POST">
				|
					<input type="hidden" name="user" id="user" value="$username"></input>
					<input type="hidden" name="follow" id="follow" value="$should_follow"></input>
					<button type="submit" class="nostyle" style="color:$color">$display_text</button>
			</form>
		);
	}

	print <<individual;
				<p>
					<h4 style="color:Silver">
						<img class="img-rounded" height="32" width="32" src="$profile_image" alt=""> 
						<a style="color:#0099FF" href="profile.cgi?user=$username">\@$username</a> $fullname
					</h4>
				</p>

				<p style="font-size:120%;">Following : <b>$listen_to</b> $button_follow</p>
				<p style="padding:0px" class="modal-header"></p>
individual

}




# This function helps to print out the home page.
sub build_home_page {
	my $login_form = build_login_form(); # This will get the login form.
	my $signup_form = build_signup_form(1); #This will get the sign up form.

	print <<login;
		<div class="container">
	        <h1 class="page-header"></h1>
		    <div class="row">
		        <div class="col-xs-12 col-md-8">
		        	<h2 style="color:White">
		        		Welcome to Bitter.
		        	</h2>
			    	<h3 style="color:White">
			    		Connect with your friends — and other fascinating people. Get in-the-moment updates on the things that interest you. And watch events unfold, in real time, from every angle.
			    	</h3>
			    </div>
				$login_form
			</div>
			<br/>
			<br/>
			<div class="row">
		        <div class="col-xs-12 col-md-8">
			    </div>
				$signup_form
			</div>
		</div>
login
}





# This function will help to build the login page.
sub build_login_page {
	my $login_id = $_[0];
	my $password = $_[1] || "";
	my $alert_message = $_[2] || "";

	my $login_form = build_login_form("$login_id", "$password", "$alert_message");

	print <<login;
		<div class="container">
			<div class="row">
				<h1 class="page-header">
					<center style="color:White">Log in to Bitter</center>
				</h1>
			</div>
		    <div class="row">
			    <div class="col-lg-12">
					$login_form
		        </div>
		    </div>
		    <div class="row">
		    	<div class="col-lg-12">
					<h5 style="color:White">
						New to Bitter? <a style="color:#0099FF" href="signup.cgi">Sign up now »</a>
					</h5>
				</div>
		    </div>
		</div>
login
}





# This function will help to create a login form.
sub build_login_form{
	my $login_id = $_[0] || "";
	my $password = $_[1] || "";
	my $alert_message = $_[2] || "";
	
	my $error_user = "";
	my $error_pass = "";

	if (defined $alert_message && length($alert_message) > 0){
		$error_pass = "has-error";

		if (defined $password && length($password) > 0){
			$error_user = "has-error";
		}
	}

	return <<login;
				<div class="col-xs-12 col-md-4 img-rounded" style="background-color:White">
					<p><h5 style="color:Red" name="error_message_sign_in" id="error_message_sign_in">$alert_message</h5></p>
					<p>
						<form action="login.cgi" method="POST" onSubmit="return validation_sign_in(this)">
							<div class="form-group $error_user" id="div-user-sign-in" name="div-user-sign-in">
								<input type="text" class="form-control" id="log_id" name="log_id" placeholder="Email or Username" value="$login_id">
							</div>
							<div class="form-group $error_pass" id="div-pass-sign-in" name="div-pass-sign-in">
								<input type="password" class="form-control" id="log_pass" name="log_pass" placeholder="Password">
							</div>
							<div class="checkbox">
								<label>
									<a style="color:#0099FF" href="action.cgi?a=fp">Forget Password?</a>
								</label>
							</div>
							<button type="submit" class="btn btn-default">Log in</button>
						</form>
					</p>
				</div>
login
}





# This function will help to display a sign up page
sub build_signup_page {
	my $username = $_[0];
	my $email = $_[1] || "";
	my $password = $_[2] || "";

	my $full_name = $_[3] || "";
	my $latitude = $_[4] || "";
	my $longitude = $_[5] || "";
	my $suburb = $_[6] || "";

	my $alert_message = $_[7] || "";

	my $signup_form = build_signup_form(0, $username, $email, $password, $full_name, $latitude, $longitude, $suburb, $alert_message);

	print <<signup;
		<div class="container">
			<div class="row">
				<h1 class="page-header">
					<center style="color:White">Sign Up to Bitter</center>
				</h1>
			</div>
		    <div class="row">
			    <div class="col-lg-12">
					$signup_form
		        </div>
		    </div>
		</div>
signup
}


# This function will help to create a sign up form.
sub build_signup_form{
	my $show_compulsory = $_[0] || 0;

	my $username = $_[1];
	my $email = $_[2] || "";
	my $password = $_[3] || "";

	my $full_name = $_[4] || "";
	my $latitude = $_[5] || "";
	my $longitude = $_[6] || "";
	my $suburb = $_[7] || "";

	my $alert_message = $_[8] || "";

	if ($show_compulsory){
		return <<signup;
				<div class="col-xs-12 col-md-4 img-rounded" style="background-color:White">
					<p>
						<h4>
							New to Bitter? Sign up
						</h4>
						<p><h5 style="color:Red" name="error_message_sign_up" id="error_message_sign_up"></h5></p>

						<form action="signup.cgi" method="POST" onSubmit="return validation_sign_up(this)">
							<div class="form-group" id="div-user-sign-up" name="div-user-sign-up">
								<input type="text" class="form-control" id="su_user" name="su_user" placeholder="Username">
							</div>
							<div class="form-group" id="div-email-sign-up" name="div-email-sign-up">
								<input type="text" class="form-control" id="su_em" name="su_em" placeholder="Email">
							</div>
							<div class="form-group" id="div-pass-sign-up" name="div-pass-sign-up">
								<input type="password" class="form-control" id="su_pass" name="su_pass" placeholder="Password">
							</div>
							<button type="submit" class="btn btn-default">Sign up for Bitter</button>
						</form>
					</p>
				</div>
signup
	}
	else{
		return <<signup;
				<div class="col-xs-12 col-lg-8 col-lg-offset-2 img-rounded" style="background-color:White">
					<p>
						<p><h5 style="color:Red" name="error_message_sign_up" id="error_message_sign_up">$alert_message</h5></p>

						<form action="signup.cgi" method="POST" onSubmit="return validation_sign_up(this)">
							<center>
							<div class="form-group" id="div-user-sign-up" name="div-user-sign-up">
								<input type="text" class="form-control" id="su_user" name="su_user" placeholder="Username (Compulsory)" value="$username">
							</div>
							<div class="form-group" id="div-email-sign-up" name="div-email-sign-up">
								<input type="text" class="form-control" id="su_em" name="su_em" placeholder="Email (Compulsory)" value="$email">
							</div>
							<div class="form-group" id="div-pass-sign-up" name="div-pass-sign-up">
								<input type="password" class="form-control" id="su_pass" name="su_pass" placeholder="Password (Compulsory)" value="$password">
							</div>

							<div class="form-group">
								<input type="text" class="form-control" id="fn" name="fn" placeholder="Full Name" value="$full_name">
							</div>
							<div class="form-group">
								<input type="text" class="form-control" id="sub" name="sub" placeholder="Home Suburb" value="$suburb">
							</div>

							<div class="form-group">
								<table>
									<tr>
										<td>
											<input placeholder="Home Latitude" type="text" class="form-control" name="lat" id="lat" value="$latitude">
										</td>
										<td>
											<input placeholder="Home Longitude" type="text" class="form-control" name="long" id="long" value="$longitude">
										</td>
										<td>
											<button type="button" class="btn btn-default" onclick="getLocation()">Get Location</button>
										</td>
									</tr>
								</table>
							</div>
							
							<button type="submit" class="btn btn-default">Sign up for Bitter</button>

							</center>
						</form>
					</p>
				</div>
signup
	}
}




# This function helps to create a verification message page.
sub build_verification_page {
	my $message = $_[0] || "";
	my $is_error = $_[1] || 0;

	my $color = "\#0099FF";

	if ($is_error){
		$color = "Red";
	}

	print <<verification;
		<div class="container">
			<div class="row">
				<h1 class="page-header"></h1>
			</div>
		    <div class="row">
			    <div class="col-lg-12">
					<div class="col-xs-12 col-lg-10 col-lg-offset-1 img-rounded" style="background-color:White">
						<p>	
							<center>
								<h4 style="color:$color">
									$message
								</h4>
							</center>
						</p>
					</div>
		        </div>
		    </div>
		</div>
verification
}





# This function helps to build the search page
sub build_search_page{
	my $search_value = $_[0];
	my $search_option = $_[1];

	my $check_profile = "checked";
	my $check_bleat = "";

	if ($search_option eq "bleat"){
		$check_profile = "";
		$check_bleat = "checked";
	}

	print <<SearchForm;
	<div class="container">
	    <div class="row page-header"></div>
	    <div class="row">
			<center>
		        <div class="col-lg-6 col-lg-offset-3">
					<h1 class="">
						<p><h5 style="color:Red" name="error_message_search" id="error_message_search"></h5></p>
						<form role="search" action="search.cgi" method="GET" onSubmit="return validation_search(this)">
							<div class="form-group" id="div-search" name="div-search">
								<input placeholder="Search for Username or Full Name or Bleat" type="text" class="form-control" name="q" id="q" value="$search_value">
								<br/>
							</div>	
							<div class="form-group">
								<div class="radio" style="color:white">
									<label style="margin-right:10px">
										<input type="radio" name="qo" id="optionProfile" value="profile" $check_profile>
										Search for Profile
									</label>

									<label style="margin-left:10px">
										<input type="radio" name="qo" id="optionBleat" value="bleat" $check_bleat>
										Search for Bleat
									</label>
								</div>
								<button type="submit" class="btn btn-default">Search</button>	
							</div>
						</form>
					</h1>		
		        </div>
			</center>
	    </div>
SearchForm
}





# This function helps to create the start of search result HTML tags
sub build_search_start {
	print <<Search_Start;
				<div class="col-xs-10 col-lg-10 col-lg-offset-1 col-xs-offset-1 img-rounded" style="background-color:White">
Search_Start
}




# This function helps to create the end of search result HTML tags
sub build_search_end {
	print <<Search_End
		</div>
	</div>
Search_End
}




# This function will help to print all of the search result in a nicely formatted HTML.
sub build_individual_search {
	my $search_value	= $_[0];
	my $search_option	= $_[1];

	my $username 		= $_[2];
	my $fullname 		= $_[3];
	my $profile_image	= "./" . $_[4];
	my $listen_to		= $_[5];
	my $is_personal		= $_[6];
	my $is_follow		= $_[7];

	my $button_follow = "";

	if (!$is_personal){
		my $display_text = "Follow";
		my $should_follow = "yes";
		my $color = "\#0099FF";

		if ($is_follow){
			$display_text = "Unfollow";
			$should_follow = "no";
			$color = "Red";
		}

		$button_follow = qq(
			<form class="form-horizontal" action="search.cgi" method="POST">
				|
					<input type="hidden" name="q" id="q" value="$search_value"></input>
					<input type="hidden" name="qo" id="qo" value="$search_option"></input>
					<input type="hidden" name="user" id="user" value="$username"></input>
					<input type="hidden" name="follow" id="follow" value="$should_follow"></input>
					<button type="submit" class="nostyle" style="color:$color">$display_text</button>
			</form>
		);
	}

	print <<individual;
				<p>
					<h4 style="color:Silver">
						<img class="img-rounded" height="32" width="32" src="$profile_image" alt=""> 
						<a style="color:#0099FF" href="profile.cgi?user=$username">\@$username</a> $fullname
					</h4>
				</p>

				<p style="font-size:120%;">Following : <b>$listen_to</b> $button_follow</p>
				<p style="padding:0px" class="modal-header"></p>

individual
}





# This function will help to print out a message when there is no result found in a search
sub build_not_found_search{
	    print <<NotFound;
				<p>
					<h3 style="color:Red;margin-bottom:20px">
						<center>Sorry, <a style="color:#0099FF" href="home.cgi">Bitter</a> can't find what you are looking for...</center>
					</h3>
					
				</p>
NotFound
}





# This function will help out to print the write page.
sub build_write{
	my $reply_to = $_[0] || "";
	my $message = $_[1] || "";
	my $bleat_message = $_[2] || "";
	my $bleat_latitude = $_[3] || "";
	my $bleat_longitude = $_[4] || "";

	my $color = "Red";
	if ($message =~ /success/i){
		$color = "#0099FF";
	}

	my $reply_to_box = "";
	if ($reply_to){
		Bitter::build_database();

		if (defined $Bitter::global_all_user_bleats{"$reply_to"}){
			my %user_bleat 		= %{ $Bitter::global_all_user_bleats{"$reply_to"} };
			my $username 		= $user_bleat{"username"} || "";
			my %user_profile 	= %{ $Bitter::global_all_users{lc("$username")} };

			my $fullname 		= $user_profile{"full_name"} || "";
			my $profile_image	= "./" . $user_profile{"profile_pic"} || "";

			my $bleat 			= $user_bleat{"bleat"} || "";
			my $time 			= $user_bleat{"time"} || "";
			$time 				= convert_epoch_to_bleat_date($time);
			my $latitude		= $user_bleat{"latitude"} || "";
			my $longitude		= $user_bleat{"longitude"} || "";

			my $google_maps 	= help_build_location_info("Location", $latitude, $longitude);
			
			$bleat =~ s/([^\:\&]?)\#(\w+)/$1<a style=\"color:\#0099FF\" href=\".\/search\.cgi\?q\=\%23$2&qo\=bleat\">\#$2<\/a>/g; # Handle hashtags and special characters
			# Hashtags above will redirect to search page (search.cgi) upon clicking.
			$bleat =~ s/@([A-Za-z0-9\_\-\.]+)/<a style=\"color:\#0099FF\" href=\"\.\/profile\.cgi\?user\=$1\">\@$1<\/a>/g;
			
			if (length($google_maps) > 0){
				$google_maps = "| $google_maps";
			}

			$reply_to_box = <<individual;
						<div class="col-lg-10 col-lg-offset-1 col-xs-10 col-xs-offset-1 img-rounded" style="background-color:White">
							<p>
								<h4 style="color:Silver">
									<img class="img-rounded" height="32" width="32" src="$profile_image" alt=""> 
										<a style="color:#0099FF" href="profile.cgi?user=$username">$fullname</a> \@$username
								</h4>
							</p>
							<p>
								<h4 style="color:SlateGray">$bleat</h4>
							</p>
							<p>$time $google_maps</p>
						</div>

						<div class="col-lg-10 col-lg-offset-1 col-xs-10 col-xs-offset-1">
							<br/><br/>
						</div>
individual
	
			if (length($bleat_message) == 0){
				$bleat_message .= "\@$username ";
			} 
		}
	}



	print <<Write;
		<div class="container">
		    <div class="row">
			        <div class="col-lg-12">
						<h1 class="page-header">
							<center style="color:White"></center>
						</h1>

						$reply_to_box

						<div class="col-lg-10 col-lg-offset-1 col-xs-10 col-xs-offset-1 img-rounded" style="background-color:White">
							<center>
								<p><h4 style="color:$color" name="error_message_search" id="error_message_search">$message</h4></p>

								<form action="write.cgi" method="POST" enctype="multipart/form-data" onSubmit="return validation_write(this)">
									<input type="hidden" name="rt" id="rt" value="$reply_to"></input>

									<div class="form-group" id="div-write" name="div-write">
										<textarea type="text" class="form-control" style="resize: none; margin-bottom:20px; margin-top:20px;" rows="4" name="bm" id="bm" placeholder="What's Happening?">$bleat_message</textarea>
									</div>		

									<div class="form-group" id="div-upload" name="div-upload">
										<p><input accept="image/jpg, image/jpeg, video/mp4" type="file" id="bleat_up_1" name="bleat_up_1" ></p>
										<p><input accept="image/jpg, image/jpeg, video/mp4" type="file" id="bleat_up_2" name="bleat_up_2" ></p>
										<p><input accept="image/jpg, image/jpeg, video/mp4" type="file" id="bleat_up_3" name="bleat_up_3" ></p>
										<p><input accept="image/jpg, image/jpeg, video/mp4" type="file" id="bleat_up_4" name="bleat_up_4" ></p>
										<p>Only JPG and MP4 files are allowed !</p>
									</div>

									<input type="checkbox" name="cl" id="cl">    Add Your Location    <img class="img-rounded" height="22" width="22" src="./assets/location.png" alt=""> </input>
									
									<br/>
									<div class="form-group" id="div-location" name="div-location" style="display:none">
										<table>
											<tr>
												<td>
													<input placeholder="Latitude" type="text" class="form-control" name="lat" id="lat" value="$bleat_latitude">
												</td>
												<td>
													<input placeholder="Longitude" type="text" class="form-control" name="long" id="long" value="$bleat_longitude">
												</td>
												<td>
													<button type="button" class="btn btn-default" onclick="getLocation()">Get Location</button>
												</td>
											</tr>
										</table>
									</div>

									<h5 id="character_count" name"character_count"><b style="color:#0099FF">142 characters left</b></h5>

									<button type="submit" class="btn btn-default glyphicon glyphicon-pencil" style="margin-bottom:10px;">Bleat</button>
								</form>
							</center>
						</div>
					</div>
		        </div>
		    </div>
		</div>

Write
}





# This function helps to create the pagination bar at the bottom of the page
sub build_pagination_bar{
	my $page_name = $_[0];
	my $page_number = $_[1];
	my $result_limit = $_[2];
	my $total_page_number = $_[3];
	my $pagination_limit = $_[4];
	my $additional_data = $_[5] || "";

	my $start_pagination = 1;
	my $end_pagination = $total_page_number;

	my @page_format = ();
	my $url_link = "./$page_name\.cgi\?";

	if ($additional_data && length($additional_data) > 0){
		$url_link .= $additional_data;
		$url_link .= "\&";
	}

	if ($total_page_number > $pagination_limit){ # Logic for getting the navigation bar limited up to specified number
		my $temporary_number = $pagination_limit - 1;
		$start_pagination = $page_number + 1;
		$end_pagination = $page_number + 1;

		while ($temporary_number > 0){

			if ($start_pagination > 1 && $temporary_number > 0){
				--$start_pagination;
				--$temporary_number;
			}

			if ($end_pagination < $total_page_number && $temporary_number > 0){
				++$end_pagination;
				--$temporary_number;
			}
		}
	}

	if (($page_number + 1) > 1){ # Previous Button
		# Because page_number starts from 0 and the required link is from 1 to ... so it must be added by added by 1 and subtracted by 1 again
		my $new_link = "$url_link" . "n=$page_number";
		push(@page_format, qq(<a class="pagination" href="$new_link">Previous</a>));
	}
	else{
		push(@page_format, qq(<b class="pagination-bold">Previous</b>));
	}


	# The link number that is to be used
	for (my $i = $start_pagination; $i <= $end_pagination; $i++){ # Number Pagination
		if ($i == ($page_number + 1)){
			push(@page_format, qq(<b class="pagination-bold">$i</b>));
		}
		else{
			my $new_link = "$url_link" . "n=$i";
			push(@page_format, qq(<a class="pagination" href="$new_link">$i</a>));
		}
	}

	if (($page_number + 1) < $total_page_number){
		my $temp_page_number = $page_number + 2; # Because page_number starts from 0 and the required link is from 1 to ... so it must be added by 1 twice
		my $new_link = "$url_link" . "n=$temp_page_number";
		push(@page_format, qq(<a class="pagination" href="$new_link">Next</a>));
	}
	else{
		push(@page_format, qq(<b class="pagination-bold">Next</b>));
	}


	my $pagination_format = join(" | ", @page_format);

	print <<pagination;
		<div class="container">
		    <div class="row">
		    	<br/>
		    	<br/>
				<div class="col-lg-10 col-lg-offset-1 col-xs-10 col-xs-offset-1 img-rounded" style="background-color:White" id="bleat-toggle-page-div">
					<center>
						<p>
							$pagination_format
						</p>
					</center>
		    	</div>
		    </div>
		</div>
pagination
}





# This function will help to creat the account settings page
sub build_account_settings_page{
	my $username = $_[0];

    my $email = $_[1];
    my $full_name = $_[2];
    my $suburb = $_[3];

    my $latitude = $_[4];
    my $longitude = $_[5];
    my $prof_text = $_[6];

    my $notify_mention = $_[7];
    my $notify_reply = $_[8];
    my $notify_new_listen = $_[9];

    my $alert_message_change_info = $_[10];
    my $alert_message_change_pass = $_[11];

    my $alert_message_change_pp = $_[12];
    my $alert_message_change_bg = $_[13];

    my $alert_message_change_acc = $_[14];

    my $color = "Red";

    if ($alert_message_change_info =~ /success/i || $alert_message_change_pass =~ /success/i 
    	|| $alert_message_change_pp =~ /success/i || $alert_message_change_bg =~ /success/i
    	|| $alert_message_change_acc =~ /success/i){
    	$color = "Green";
    }

    my $suspend_unsuspend = "";
    my $status = $Bitter::global_all_users{lc("$username")}{"status"};


	if ($notify_mention eq "on"){
		$notify_mention = "checked";
	}
	else{
		$notify_mention = "unchecked";
	}


	if ($notify_reply eq "on"){
		$notify_reply = "checked";
	}
	else{
		$notify_reply = "unchecked";
	}


	if ($notify_new_listen eq "on"){
		$notify_new_listen = "checked";
	}
	else{
		$notify_new_listen = "unchecked";
	}


    if ($status eq "suspend"){
    	$suspend_unsuspend = <<suspend;
	    	<form action="settings.cgi" method="POST">
				<div class="form-group">
					<button type="submit" class="btn btn-default lightgray_btn">Unsuspend Account</button>
					<input type="hidden" class="form-control" id="act" name="act" value="unsuspend">
				</div>
			</form>
suspend
    }
    else{
    	$suspend_unsuspend = <<suspend;
	    	<form action="settings.cgi" method="POST">
				<div class="form-group">
					<button type="submit" class="btn btn-default lightgray_btn">Suspend Account</button>
					<input type="hidden" class="form-control" id="act" name="act" value="suspend">
				</div>
			</form>
suspend
    }


	print <<settings;
		<div class="container">
			<div class="row">
				<h1 class="page-header">
					<center style="color:White">Account Settings</center>
				</h1>
			</div>
		    <div class="row">
			    <div class="col-lg-12">

					<div class="col-xs-12 col-lg-8 col-lg-offset-2 img-rounded" style="background-color:White">
					<p>
						<p><h5 style="color:$color" name="error_message_ac" id="error_message_ac">$alert_message_change_info</h5></p>

						<form action="settings.cgi" method="POST" onSubmit="return validation_ac(this);">
							
							<center>
								<p><h5 style="color:#0099FF">Account Information</h5></p>
							</center>

							<div class="form-group">
								<h4 style="color:#0099FF">Username : \@$username</h4>
							</div>

							<center>
								<div class="form-group" id="div-email-ac" name="div-email-ac">
									<input type="text" class="form-control" id="ac_em" name="ac_em" placeholder="Email (Compulsory)" value="$email">
								</div>

								<div class="form-group">
									<input type="text" class="form-control" id="fn" name="fn" placeholder="Full Name" value="$full_name">
								</div>
								<div class="form-group">
									<input type="text" class="form-control" id="sub" name="sub" placeholder="Home Suburb" value="$suburb">
								</div>

								<div class="form-group">
									<table>
										<tr>
											<td>
												<input placeholder="Home Latitude" type="text" class="form-control" name="lat" id="lat" value="$latitude">
											</td>
											<td>
												<input placeholder="Home Longitude" type="text" class="form-control" name="long" id="long" value="$longitude">
											</td>
											<td>
												<button type="button" class="btn btn-default" onclick="getLocation()">Get Location</button>
											</td>
										</tr>
									</table>
								</div>
							
								<div class="form-group">
									<textarea type="text" class="form-control" style="resize: none; margin-bottom:20px; margin-top:20px;" rows="4" name="pt" id="pt" placeholder="Describe Yourself, Your Interests or Anything you want.">$prof_text</textarea>
									Note: You can use tags such as &lt;b&gt; , &lt;i&gt; , &lt;u&gt; and many more to your description.
								</div>		
							
								<input type="hidden" class="form-control" id="act" name="act" value="c_acci">

								<button type="submit" class="btn btn-default">Save</button>
							</center>
						</form>
					</p>
					</div>
		        </div>
		    </div>

		    <br/>
		    <div class="row">
			    <div class="col-lg-12">
				    <div class="col-xs-12 col-lg-8 col-lg-offset-2 img-rounded" style="background-color:White">
						<p>
							<center>
								<p><h5 style="color:#0099FF">Change Password</h5></p>

								<p><h5 style="color:$color" name="error_message_ps" id="error_message_ps">$alert_message_change_pass</h5></p>
						
								<form action="settings.cgi" method="POST" onSubmit="return validation_pass(this)">					
									<div class="form-group" id="div-cpo-ps" name="div-cpo-ps">
										<input type="password" class="form-control" id="cpo" name="cpo" placeholder="Old Password (Compulsory)" value="">
									</div>
									<div class="form-group" id="div-cpn-ps" name="div-cpn-ps">
										<input type="password" class="form-control" id="cpn" name="cpn" placeholder="New Password (Compulsory)" value="">
									</div>
									<div class="form-group" id="div-cpnv-ps" name="div-cpnv-ps">
										<input type="password" class="form-control" id="cpnv" name="cpnv" placeholder="Verify New Password (Compulsory)" value="">
									</div>

									<input type="hidden" class="form-control" id="act" name="act" value="c_pass">

									<button type="submit" class="btn btn-default">Change Password</button>
								</form>
							</center>
						</p>
					</div>
				</div>
			</div>
	    	
			<br/>
		    <div class="row">
			    <div class="col-lg-12">
				    <div class="col-xs-12 col-lg-8 col-lg-offset-2 img-rounded" style="background-color:White">
						<p>
							<center>
								<p><h5 style="color:#0099FF">Change Profile Picture</h5></p>

								<p><h5 style="color:$color" name="error_message_ac" id="error_message_ac">$alert_message_change_pp</h5></p>
						
								<form action="settings.cgi" method="POST" enctype="multipart/form-data">
									<div class="form-group">
										<p><input accept="image/jpg, image/jpeg" type="file" id="new_img_profile" name="new_img_profile" ></p>
										<p>Only JPG files are allowed !</p>
										<button type="submit" class="btn btn-default">Change Profile Picture</button>
										<input type="hidden" class="form-control" id="act" name="act" value="c_img_pp">
									</div>
								</form>
								<form action="settings.cgi" method="POST">
									<div class="form-group">
										<button type="submit" class="btn btn-default">Delete Profile Picture</button>
										<input type="hidden" class="form-control" id="act" name="act" value="d_img_pp">
									</div>
								</form>
							</center>
						</p>
					</div>
				</div>
			</div>

			<br/>
		    <div class="row">
			    <div class="col-lg-12">
				    <div class="col-xs-12 col-lg-8 col-lg-offset-2 img-rounded" style="background-color:White">
						<p>
							<center>
								<p><h5 style="color:#0099FF">Change Background Image</h5></p>

								<p><h5 style="color:$color" name="error_message_ac" id="error_message_ac">$alert_message_change_bg</h5></p>
						
								<form action="settings.cgi" method="POST" enctype="multipart/form-data">
									<div class="form-group">
										<p><input accept="image/jpg, image/jpeg" type="file" id="new_img_back" name="new_img_back" ></p>
										<p>Only JPG files are allowed !</p>
										<button type="submit" class="btn btn-default">Change Background Image</button>
										<input type="hidden" class="form-control" id="act" name="act" value="c_img_bg">
									</div>
								</form>
								<form action="settings.cgi" method="POST">
									<div class="form-group">
										<button type="submit" class="btn btn-default">Delete Background Image</button>
										<input type="hidden" class="form-control" id="act" name="act" value="d_img_bg">
									</div>
								</form>
							</center>
						</p>
					</div>
				</div>
			</div>

			<br/>
		    <div class="row">
			    <div class="col-lg-12">
				    <div class="col-xs-12 col-lg-8 col-lg-offset-2 img-rounded" style="background-color:White">
						<p>
							<center>
								<p><h5 style="color:#0099FF">Change Notification Settings</h5></p>
						
								<form action="settings.cgi" method="POST">
									<div class="form-group">
										<p><input type="checkbox" name="notify_mention" $notify_mention> Someone mentioned Me in a Bleat</input></p>
										
										<p><input type="checkbox" name="notify_reply" $notify_reply> Someone reply My Bleat</input></p>
										
										<p><input type="checkbox" name="notify_new_listen" $notify_new_listen> Someone listens to me</input></p>
										<input type="hidden" class="form-control" id="act" name="act" value="c_notif">
										
										<p><button type="submit" class="btn btn-default">Save</button></p>
									</div>
								</form>
							</center>
						</p>
					</div>
				</div>
			</div>

		    <br/>
		    <div class="row">
			    <div class="col-lg-12">
					<div class="col-xs-12 col-lg-8 col-lg-offset-2 img-rounded" style="background-color:White">
						<center>
						<p></p>
						<p>
							<p><h5 style="color:$color" name="error_message_ac" id="error_message_ac">$alert_message_change_acc</h5></p>

							$suspend_unsuspend
						</p>
						<p>
							<form action="settings.cgi" method="POST">
								<div class="form-group">
									<button type="submit" class="btn btn-default red_btn">Delete Account</button>
									<input type="hidden" class="form-control" id="act" name="act" value="delete">
								</div>
							</form>
						</p>
						</center>
					</div>
				</div>
			</div>
		</div>
settings
}




# This function helps to build the recover password page
sub build_recover_password {
	my $alert_message = $_[0] || "";

	print <<Recover;
	<div class="container">
	    <div class="row">
	        <div class="col-lg-12">
		
				<h1 class="page-header">
					<center style="color:White">Recover Password</center>
				</h1>
				<center>
					<div class="col-xs-12 col-lg-8 col-lg-offset-2 img-rounded" style="background-color:White">
					<p>
						<p><h5 style="color:Red" name="error_message_rp" id="error_message_rp">$alert_message</h5></p>
						<p>
							<center>						
								<form action="action.cgi" method="POST" onSubmit="return validation_rp(this)">					
									<div class="form-group" id="div-id-rp" name="div-id-rp">
										<input type="text" class="form-control" id="rp_id" name="rp_id" placeholder="Your Username or Email Address">
									</div>

									<input type="hidden" class="form-control" id="a" name="a" value="fp">

									<button type="submit" class="btn btn-default">Change Password</button>
								</form>
							</center>
						</p>

					</div>
				</center>
	        </div>
	    </div>
	</div>
Recover
}





# THis function helps to create the change password page when doing a password recovery.
sub build_change_password_recovery {
	my $username = $_[0];
	my $recover_code = $_[1];

	print <<Recover
	<div class="container">
		<div class="row">
			    <div class="col-lg-12">
				    <h1 class="page-header">
						<center style="color:White">Recover Password</center>
					</h1>
				    <div class="col-xs-12 col-lg-8 col-lg-offset-2 img-rounded" style="background-color:White">
						<p>
							<center>	
								<p><h5 style="color:Red" name="error_message_cp" id="error_message_cp"></h5></p>
					
								<form action="action.cgi" method="POST" onSubmit="return build_check_change_password_script(this)">					
									<div class="form-group" id="div-cpn-ac" name="div-cpn-ac">
										<input type="password" class="form-control" id="cpn" name="cpn" placeholder="New Password (Compulsory)" value="">
									</div>

									<input type="hidden" class="form-control" id="a" name="a" value="fp">
									<input type="hidden" class="form-control" id="rp_id" name="rp_id" value="$username">
									<input type="hidden" class="form-control" id="rec" name="rec" value="$recover_code">

									<button type="submit" class="btn btn-default">Change Password</button>
								</form>
							</center>
						</p>
					</div>
				</div>
			</div>
		</div>
Recover
}



# This function helps to build a delete confirmation bleat page .
sub build_delete_bleat_page {
	my $bleat_id = $_[0];

	if ($bleat_id){
		$bleat_id = lc("$bleat_id");

		if (defined $Bitter::global_all_user_bleats{"$bleat_id"}){
			my %user_bleat 		= %{ $Bitter::global_all_user_bleats{"$bleat_id"} };
			my $username 		= $user_bleat{"username"} || "";
			my %user_profile 	= %{ $Bitter::global_all_users{lc("$username")} };

			my $fullname 		= $user_profile{"full_name"} || "";
			my $profile_image	= "./" . $user_profile{"profile_pic"} || "";

			my $bleat 			= $user_bleat{"bleat"} || "";
			my $time 			= $user_bleat{"time"} || "";
			$time 				= convert_epoch_to_bleat_date($time);

			my $latitude		= $user_bleat{"latitude"} || "";
			my $longitude		= $user_bleat{"longitude"} || "";
			my $in_reply		= $user_bleat{"in_reply_to"} | "";

			my $google_maps 	= help_build_location_info("Location", $latitude, $longitude);
			
			$bleat =~ s/([^\:\&]?)\#(\w+)/$1<a style=\"color:\#0099FF\" href=\".\/search\.cgi\?q\=\%23$2&qo\=bleat\">\#$2<\/a>/g; # Handle hashtags and special characters
			# Hashtags above will redirect to search page (search.cgi) upon clicking.
			$bleat =~ s/@([A-Za-z0-9\_\-\.]+)/<a style=\"color:\#0099FF\" href=\"\.\/profile\.cgi\?user\=$1\">\@$1<\/a>/g;
			
			if (length($google_maps) > 0){
				$google_maps = "| $google_maps";
			}

			if (defined $in_reply){
				$in_reply = "| <a style=\"color:\#0099FF\" href=\"conversation.cgi?id=$bleat_id\">See Conversation</a>";
			}
			else{
				$in_reply = "";
			}

			print <<individual;
			<div class="container">
			    <div class="row">
			        <div class="col-lg-12">
						<h1 class="page-header">
							<center style="color:White"></center>
						</h1>

						<div class="col-lg-10 col-lg-offset-1 col-xs-10 col-xs-offset-1 img-rounded" style="background-color:White">
							<p>
								<h4 style="color:Silver">
									<img class="img-rounded" height="32" width="32" src="$profile_image" alt=""> 
										<a style="color:#0099FF" href="profile.cgi?user=$username">$fullname</a> \@$username
								</h4>
							</p>
							<p>
								<h4 style="color:SlateGray">$bleat</h4>
							</p>
							<p>$time $google_maps $in_reply</p>
							<p>Are you sure you want to delete this Bleat?</p>
							<form method="GET" action="action.cgi">
								<input type="hidden" class="form-control" name="a" id="a" value="db">
								<input type="hidden" class="form-control" name="b_id" id="b_id" value="$bleat_id">
								<input type="hidden" class="form-control" name="del" id="del" value="yes">

								<button type="submit" class="btn btn-default red_btn">Delete Bleat</button>
							</form>
							<br/>
						</div>

						<div class="col-lg-10 col-lg-offset-1 col-xs-10 col-xs-offset-1">
							<br/><br/>
						</div>

						
					</div>
				</div>
			</div>
individual

		} 
	}
}



# Script Helper

# Create a script for writing number of characters in the write (new) bleat page. It will automatically update the number
# of characters when user starts typing.
sub build_write_count_script{
	return q(
	<script type="text/javascript">
		counter = function() {
		    var totalChars_left = get_count_char();
   
		    if (totalChars_left.length <= 15){
			    $('#character_count').html("<b style=\"color:Red\">" + totalChars_left + " characters left</b>");
		    }
		    else{
			    $('#character_count').html("<b style=\"color:#0099FF\">" + totalChars_left + " characters left</b>");
		    }
		};

		function get_count_char(){
		    var value = $('#bm').val();//.trim();
		    var encode = encodeURIComponent(value).match(/%[89ABab]/g);
		    var val_length = value.length ;//+ (encode ? encode.length : 0);

		    var totalChars_left = 142 - val_length;

		    return totalChars_left;
		}

		$(document).ready(function() {
		    var totalChars_left = get_count_char();
   
		    if (totalChars_left.length <= 15){
			    $('#character_count').html("<b style=\"color:Red\">" + totalChars_left + " characters left</b>");
		    }
		    else{
			    $('#character_count').html("<b style=\"color:#0099FF\">" + totalChars_left + " characters left</b>");
		    }

			// Detect Change to Field
		    $('#bm').change(counter);
		    $('#bm').keydown(counter);
		    $('#bm').keypress(counter);
		    $('#bm').keyup(counter);
		    $('#bm').blur(counter);
		    $('#bm').focus(counter);

		    // Detect Copy, Past & Cut
			$("#bm").bind('copy', counter);
		    $("#bm").bind('paste', counter);
		    $("#bm").bind('cut', counter);
		});
	</script>
);
}

# This function helps to create a script that checks whether the necessary fields for log in is being filled and 
# whether the login id (username or email) is within 5 to 32 characters long
sub build_check_login_script {
	return q(
		<script type="text/javascript">
			function validation_sign_in(form){									
				var login_id = form.log_id.value.trim();
				login_id = login_id.replace(" ", "");
				var login_password = form.log_pass.value.trim();

				if(login_id.length == 0){
					document.getElementById("error_message_sign_in").innerHTML = "Please enter your Login ID !";
					document.getElementById("div-user-sign-in").className = "form-group has-error";
					document.getElementById("div-pass-sign-in").className = "form-group";

					form.log_id.focus();
					return false;
				}
				else if (login_password.length == 0){
					document.getElementById("error_message_sign_in").innerHTML = "Please enter your Password !";
					document.getElementById("div-user-sign-in").className = "form-group";
					document.getElementById("div-pass-sign-in").className = "form-group has-error";

					form.log_pass.focus();
					return false;
				}
				else if (login_id.length > 32 || login_id.length < 5){
					document.getElementById("error_message_sign_in").innerHTML = "Username must be 5 to 32 characters !";
					document.getElementById("div-user-sign-in").className = "form-group has-error";
					document.getElementById("div-pass-sign-in").className = "form-group has-error";

					form.log_id.focus();
					return false;
				}

				return true;
			};	
		</script>
);
}

# This function helps to create a script that checks whether the necessary fields for sign up is filled in.
sub build_check_sign_up_script {
	return q(
		<script type="text/javascript">
			function validation_sign_up(form){		
				var signup_username = form.su_user.value.trim();
				var signup_email = form.su_em.value.trim();
				var signup_password = form.su_pass.value.trim();

				if(signup_username.length == 0){
					document.getElementById("error_message_sign_up").innerHTML = "Please enter your Username !";
					document.getElementById("div-user-sign-up").className = "form-group has-error";
					document.getElementById("div-email-sign-up").className = "form-group";
					document.getElementById("div-pass-sign-up").className = "form-group";

					form.su_user.focus();
					return false;
				}
				else if (signup_email.length == 0){
					document.getElementById("error_message_sign_up").innerHTML = "Please enter your Email Address !";
					document.getElementById("div-user-sign-up").className = "form-group";
					document.getElementById("div-email-sign-up").className = "form-group has-error";
					document.getElementById("div-pass-sign-up").className = "form-group";

					form.su_em.focus();
					return false;
				}
				else if (signup_password.length == 0){
					document.getElementById("error_message_sign_up").innerHTML = "Please enter your Password !";
					document.getElementById("div-user-sign-up").className = "form-group";
					document.getElementById("div-email-sign-up").className = "form-group";
					document.getElementById("div-pass-sign-up").className = "form-group has-error";

					form.su_pass.focus();
					return false;
				}
				else if (signup_username.length > 16 || signup_username.length < 5){
					document.getElementById("error_message_sign_up").innerHTML = "Username must be 5 to 16 characters !";
					document.getElementById("div-user-sign-up").className = "form-group has-error";
					document.getElementById("div-email-sign-up").className = "form-group";
					document.getElementById("div-pass-sign-up").className = "form-group";

					form.su_user.focus();
					return false;
				}
				else if (signup_password.length < 5){
					document.getElementById("error_message_sign_up").innerHTML = "Password must be at least 5 characters !";
					document.getElementById("div-user-sign-up").className = "form-group";
					document.getElementById("div-email-sign-up").className = "form-group";
					document.getElementById("div-pass-sign-up").className = "form-group has-error";

					form.su_pass.focus();
					return false;
				}

				return true;
			};

		</script>
);
}

# This function helps to create a script that checks whether the necessary fields for recover password is filled in.
sub build_check_recover_password_script {
	return q(
		<script type="text/javascript">
			function validation_rp(form){		
				var rp_identification = form.rp_id.value.trim();

				if (rp_identification.length == 0){
					document.getElementById("error_message_rp").innerHTML = "Please enter your Username or Email Address !";
					document.getElementById("div-id-rp").className = "form-group has-error";

					form.rp_id.focus();
					return false;
				}
				
				return true;
			};

		</script>
);
}

# This function helps to create a script that checks whether the necessary fields for updating account information is filled in.
sub build_check_update_info_script {
	return q(
		<script type="text/javascript">
			function validation_ac(form){		
				var ac_email = form.ac_em.value.trim();

				if (ac_email.length == 0){
					document.getElementById("error_message_ac").innerHTML = "Please enter your Email Address !";
					document.getElementById("div-email-ac").className = "form-group has-error";

					form.ac_em.focus();
					return false;
				}
				
				return true;
			};

		</script>
);
}

# This function helps to create a script that checks whether the necessary fields for updating password is filled in.
sub build_check_update_password_script {
	return q(
		<script type="text/javascript">
			function validation_pass(form){		
				var cpo = form.cpo.value.trim();
				var cpn = form.cpn.value.trim();
				var cpnv = form.cpnv.value.trim();

				if (cpo.length == 0){
					document.getElementById("error_message_ps").innerHTML = "Please enter your Old Password !";
					document.getElementById("div-cpo-ps").className = "form-group has-error";

					form.cpo.focus();
					return false;
				}
				else if (cpn.length == 0){
					document.getElementById("error_message_ps").innerHTML = "Please enter your New Password !";
					document.getElementById("div-cpn-ps").className = "form-group has-error";

					form.cpn.focus();
					return false;
				}
				else if (cpnv.length == 0){
					document.getElementById("error_message_ps").innerHTML = "Please enter verify your New Password !";
					document.getElementById("div-cpnv-ps").className = "form-group has-error";

					form.cpnv.focus();
					return false;
				}
				
				return true;
			};

		</script>
);
}

# This function helps to create a script that checks whether the necessary fields for changing password is filled in.
sub build_check_change_password_script {
	return q(
		<script type="text/javascript">
			function build_check_change_password_script(form){		
				var cpn = form.cpn.value.trim();

				if (cpn.length == 0){
					document.getElementById("error_message_cp").innerHTML = "Please enter your new Password !";
					document.getElementById("div-cpn-ac").className = "form-group has-error";

					form.cpn.focus();
					return false;
				}
				
				return true;
			};

		</script>
);
}

# This function helps to create a script that checks whether the necessary fields for searching is filled in.
sub build_check_search_script {
	return q(
		<script type="text/javascript">
			function validation_search(form){	
				var search_value = form.q.value.trim();

				if(search_value.length == 0){
					document.getElementById("error_message_search").innerHTML = "Please enter a value to search !";
					document.getElementById("div-search").className = "form-group has-error";

					form.q.focus();
					return false;
				}
				else if(search_value.length < 2){
					document.getElementById("error_message_search").innerHTML = "Search value must be at least 2 characters !";
					document.getElementById("div-search").className = "form-group has-error";

					form.q.focus();
					return false;
				}

				return true;
			};	
		</script>
);
}

# This function helps to create a script that checks whether the necessary fields for writing a new blat is filled in.
sub build_check_write_script {
	return q(
		<script type="text/javascript">
			function validation_write(form){
				var regex_loc = /^(-)?[0-9.]+$/;

				var bleat_message = form.bm.value.trim();
				var bleat_lat = form.lat.value.trim();
				var bleat_long = form.long.value.trim();

				if(bleat_message.length == 0){
					document.getElementById("error_message_search").innerHTML = "Please enter your Bleat !";
					document.getElementById("div-write").className = "form-group has-error";

					form.bm.focus();
					return false;
				}
				else if(bleat_message.length > 142){
					document.getElementById("error_message_search").innerHTML = "Bleat must be under 142 characters !";
					document.getElementById("div-write").className = "form-group has-error";

					form.bm.focus();
					return false;
				}
				else if (document.getElementById('cl').checked) {
					if (bleat_lat.length == 0){
						document.getElementById("error_message_search").innerHTML = "Please enter your Location's Latitude !";
						form.lat.focus();
						return false; 
					}
					else if (bleat_long.length == 0){
						document.getElementById("error_message_search").innerHTML = "Please enter your Location's Longitude !";
						form.long.focus();
						return false;
					}
					else if (!bleat_lat.match(regex_loc)){
						document.getElementById("error_message_search").innerHTML = "Please enter a proper Location's Latitude !";
						form.lat.focus();
						return false; 
					}
					else if (!bleat_long.match(regex_loc)){
						document.getElementById("error_message_search").innerHTML = "Please enter a proper Location's Longitude !";
						form.long.focus();
						return false;
					}
				}

				return true;
			};	


			$(document).ready(function(){
			    $("#cl").click(function(){
					if (document.getElementById('cl').checked) {
			            document.getElementById('div-location').style.display = 'inline';
			        }
			        else{
			        	document.getElementById('div-location').style.display = 'none';
			        }
			    });

			});

		</script>
);
}

# This function helps to create a script that allows javascript to get the current location of the user.
sub build_get_location_script {
	return q(
		<script>
			function getLocation() {
			    if (navigator.geolocation) {
			        navigator.geolocation.getCurrentPosition(showPosition);
			    } else { 
			        alert("Geolocation is not supported by this browser !");
			    }
			}

			function showPosition(position) {
				document.getElementById("lat").value = position.coords.latitude;
				document.getElementById("long").value = position.coords.longitude;
			}
		</script>
);
}

# This function helps to create a script that will animate a follow / unfollow button in the profile page.
sub build_follow_hover_script{
	return q(
		<script type="text/javascript">
			$(document).ready(function(){
			    $("#follow_button").hover(function(){
			        	document.getElementById("follow_button").innerHTML = "Unfollow";
			        	document.getElementById("follow_button").style.color = "Red";
			        }, 
			        function(){
			        	document.getElementById("follow_button").innerHTML = "Following";
			        	document.getElementById("follow_button").style.color = "Black";
			    });
			});
		</script>
);
}

# This function helps to build profile toggle that allows switching between bleats and the following list of the user.
sub build_profile_toggle_script {
	return q(
		<script type="text/javascript">
			$(document).ready(function(){
			    $("#bleat-toggle").click(function(){
		            document.getElementById('bleat-toggle-div').style.display = 'inline';
		            document.getElementById('bleat-toggle-page-div').style.display = 'inline';
		        	document.getElementById('following-toggle-div').style.display = 'none';
			    });
				
				$("#following-toggle").click(function(){
		            document.getElementById('following-toggle-div').style.display = 'inline';
		        	document.getElementById('bleat-toggle-div').style.display = 'none';
		        	document.getElementById('bleat-toggle-page-div').style.display = 'none';
			    });
			});
		</script>
);
}

1;
