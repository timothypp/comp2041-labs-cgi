#!/usr/bin/perl -w
# Work by : Timothy Putra Pringgondhani (z5043683)

# validate a credit card number by calculating its
# checksum using Luhn's formula (https://en.wikipedia.org/wiki/Luhn_algorithm)

use strict;

sub luhn_checksum {
	my $number = $_[0];
	my $checksum = 0;
	my $digits = reverse $number;

	my $i = 0;
	my @enumerate = map [$i++, $_], split('', $digits); # E.g. ([0, 'a'], [1, 'b'], [2, 'c'])

	foreach my $ref (@enumerate) {
	    my($index, $digit) = @$ref;

	    my $multiplier = 1 + $index % 2;
	    my $d = int ($digit) * $multiplier;

	    if ($d > 9){
	    	$d -= 9;
	    }

	    $checksum += $d;
	}

	return $checksum
}

sub validate{
	my $credit_card = $_[0];
	my $number = $credit_card;
	$number =~ s/\D//g;

	if (length($number) != 16){
		return $credit_card . " is invalid  - does not contain exactly 16 digits\n"
	}
	elsif (luhn_checksum($number) % 10 == 0){
		return $credit_card . " is valid\n"
	}
	else{
		return $credit_card . " is invalid\n"
	}
}

unless (caller) {
	if (@ARGV > 0){
		for my $arg (@ARGV){
			chomp ($arg);
			print validate($arg);
		}
	}
}