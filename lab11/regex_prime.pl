#!/usr/bin/perl -w

# Work by : Timothy Putra Pringgondhani (z5043683)

use strict;

my $regex = qr/^1?$|^(11+?)\1+$/;

foreach my $n (1..100) {
    my $unary = 1 x $n;
    
    print "$n = $unary unary - ";

    if ($unary =~ $regex) {
        print "composite\n"
    } else {
        print "prime\n";
    }
}