#!/bin/sh
# Work by : Timothy Putra Pringgondhani (z5043683)

echo Content-type: text/html
echo

browser_ip=`env|grep REMOTE_ADDR|sed 's/^REMOTE_ADDR\=//'`
browser_hostname=`host $browser_ip 2>&1|grep -oE '[^ ]+$'|sed 's/\.$//'`
user_agent=`env|grep HTTP_USER_AGENT|sed 's/^HTTP_USER_AGENT\=//'`

cat <<eof
<!DOCTYPE html>
<html lang="en">
<head>
<title>IBrowser IP, Host and User Agent</title>
</head>
<body>
Your browser is running at IP address: <b>$browser_ip</b>
<p>
Your browser is running on hostname: <b>$browser_hostname</b>
<p>
Your browser identifies as: <b>$user_agent</b>
</body>
</html>
eof
