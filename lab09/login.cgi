#!/usr/bin/perl -w
# Work by : Timothy Putra Pringgondhani (z5043683)

use CGI qw/:all/;
use CGI::Carp qw/fatalsToBrowser warningsToBrowser/;

print header, start_html('Login');
warningsToBrowser(1);

$username = param('username') || '';
$password = param('password') || '';

chomp($username);
chomp($password);

if ("$username" && "$username" !~ /[A-Za-z0-9]/){
	print "Invalid username format. Username must be alphanumeric!", "\n";
	print end_html;
	exit(0);
}

if ("$username" && "$password"){
	if (-r "../accounts/$username/password") {
		open(F, "<../accounts/$username/password");
		$correct_password = <F>;
		chomp($correct_password);

		if ("$password" eq "$correct_password"){
			print "$username authenticated.\n";
		}
		else{
			print "Incorrect password!\n";
		}
	}
	else{
		print "Unknown username!\n"
	}
}
elsif ("$username" && !"$password"){
    print start_form, "\n";
    print hidden(-name => "username", -default => "$username", -override => 1);
    print "Password:\n", textfield('password'), "\n";
    print submit(value => Login), "\n";
    print end_form, "\n";
}
elsif (!"$username" && "$password"){
    print start_form, "\n";
    print "Username:\n", textfield('username'), "\n";
    print hidden(-name => "password", -default => "$password", -override => 1);
    print submit(value => Login), "\n";
    print end_form, "\n";
}
else {
    print start_form, "\n";
    print "Username:\n", textfield('username'), "\n";
    print "Password:\n", textfield('password'), "\n";
    print submit(value => Login), "\n";
    print end_form, "\n";
}

print end_html;
exit(0);

